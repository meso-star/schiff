/* Copyright (C) 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015, 2016 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 2 /* getopt support */

#include "schiff_args.h"
#include "schiff_version.h"
#include "schiff_optical_properties.h"
#include "schiff_geometry.h"

#include <rsys/dynamic_array_char.h>
#include <rsys/cstr.h>
#include <rsys/str.h>
#include <rsys/stretchy_array.h>

#include <stdarg.h>
#include <yaml.h>

#define MIN_NANGLES 2
#define MIN_NANGLES_INV 2

#ifdef COMPILER_CL
  #include <getopt.h>
  #define strtok_r strtok_s
#else
  #include <unistd.h>
#endif

/*******************************************************************************
 * Helper function
 ******************************************************************************/
static void
print_help(const char* binary)
{
  printf(
"Usage: %s [OPTIONS] [FILE]\n"
"Estimate the radiative properties of soft particles whose per wavelength\n"
"optical properties are stored in FILE. If not defined, optical\n"
"porperties are read from standard input. Implement the model described\n"
"in \"Monte Carlo Implementation of Schiff's Approximation for Estimating\n"
"Radiative Properties of Homogeneous, Simple-Shaped and Optically Soft\n"
"Particles: Application to Photosynthetic Micro-Organisms\" (Charon et\n"
"al. 2015).\n\n",
    binary);
  printf(
"  -a NUM_ANGLES  number of phase function scattering angles.\n"
"                 Default is %u.\n",
    SCHIFF_ARGS_NULL.nangles);
  printf(
"  -A NUM_ANGLES  number of computed inverse cumulative phase function\n"
"                 values. Default is %u.\n",
    SCHIFF_ARGS_NULL.nangles_inv);
  printf(
"  -d NUM_INSAMPS number of (incident direction, volume, ray(s)) sampling\n"
"                 for each sampled particle-shape. Default is %u.\n",
    SCHIFF_ARGS_NULL.ninsamps);
  printf(
"  -D             discard computations of the [[inverse] cumulative]\n"
"                 phase functions for large scattering angles.\n");
  printf(
"  -g NUM_SHAPES  number of sampled particle-shapes. This is actually the\n"
"                 number of realizations of the Monte Carlo algorithm.\n"
"                 Default is %u.\n",
    SCHIFF_ARGS_NULL.nrealisations);
  printf(
"  -G NUM         sampled `NUM' particle shapes with respect to the\n"
"                 defined distribution, dump their geometry and exit.\n");
  printf(
"  -h             display this help and exit.\n");
  printf(
"  -i DISTRIB     YAML file that defines the geometry distributions of\n"
"                 the soft particles.\n");
  printf(
"  -l LENGTH      characteristic length of the soft particles.\n");
  printf(
"  -n NTHREADS    hint on the number of threads to use during the\n"
"                 integration.  By default use as many threads as CPU\n"
"                 cores.\n");
  printf(
"  -o OUTPUT      write results to OUTPUT. If not defined, write results\n"
"                 to standard output.\n");
  printf(
"  -q             do not print the helper message when no FILE is\n"
"                 submitted.\n");
  printf(
"  -w A[:B]...    list of wavelengths in vacuum (expressed in micron) to\n"
"                 integrate.\n");
  printf(
"  --version      display version information and exit.\n");
  printf("\n");
  printf(
"Copyright (C) 2020, 2021 |Meso|Star> (contact@meso-star.com).\n"
"Copyright (C) 2015, 2016 CNRS. This is free software released under the\n"
"GNU GPL license, version 3 or later. You are free to change or\n"
"redistribute it under certain conditions\n"
"<http://gnu.org/licenses/gpl.html>\n");
}

static int
cmp_double(const void* op0, const void* op1)
{
  const double* a = op0;
  const double* b = op1;
  return *a < *b ? -1 : (*a > *b ? 1 : 0);
}

static INLINE void
param_release(struct schiff_param* param)
{
  ASSERT(param);

  if(param->distribution == SCHIFF_PARAM_HISTOGRAM
  && param->data.histogram.entries) {
    sa_release(param->data.histogram.entries);
  }
  param->distribution = SCHIFF_PARAM_NONE;
}

static void
geometry_release(struct schiff_geometry* geom)
{
  int i;
  ASSERT(geom);
  switch(geom->type) {
    case SCHIFF_ELLIPSOID:
    case SCHIFF_ELLIPSOID_AS_SPHERE:
      param_release(&geom->data.ellipsoid.a);
      param_release(&geom->data.ellipsoid.c);
      param_release(&geom->data.ellipsoid.radius_sphere);
      break;
    case SCHIFF_CYLINDER_AS_SPHERE:
    case SCHIFF_CYLINDER:
      param_release(&geom->data.cylinder.radius);
      param_release(&geom->data.cylinder.height);
      param_release(&geom->data.cylinder.radius_sphere);
      break;
    case SCHIFF_HELICAL_PIPE:
    case SCHIFF_HELICAL_PIPE_AS_SPHERE:
      param_release(&geom->data.helical_pipe.pitch);
      param_release(&geom->data.helical_pipe.height);
      param_release(&geom->data.helical_pipe.radius_helicoid);
      param_release(&geom->data.helical_pipe.radius_circle);
      param_release(&geom->data.helical_pipe.radius_sphere);
      break;
    case SCHIFF_SPHERE:
      param_release(&geom->data.sphere.radius);
      break;
    case SCHIFF_SUPERSHAPE:
    case SCHIFF_SUPERSHAPE_AS_SPHERE:
      FOR_EACH(i, 0, 6) {
        param_release(&geom->data.supershape.formulas[0][i]);
        param_release(&geom->data.supershape.formulas[1][i]);
      }
      param_release(&geom->data.supershape.radius_sphere);
      break;
    case SCHIFF_NONE: /* Do nothing */ break;
    default: FATAL("Unreachable code\n"); break;
  }
  geom->type = SCHIFF_NONE;
}

static res_T
parse_wavelengths(const char* str, struct schiff_args* args)
{
  size_t len;
  size_t i;
  res_T res = RES_OK;
  ASSERT(args && str);

  /* How many wavelengths are submitted */
  res = cstr_to_list_double(str, ':', NULL, &len, 0);
  if(res != RES_OK) goto error;

  /* Reserve the wavelengths memory space */
  sa_clear(args->wavelengths);
  args->wavelengths = sa_add(args->wavelengths, len);

  /* Read the wavelengths */
  res = cstr_to_list_double(optarg, ':', args->wavelengths, NULL, len);
  if(res != RES_OK) goto error;

  /* Check the validity of read wavelengths */
  FOR_EACH(i, 0, len) {
    if(args->wavelengths[i] < 0.0) {
      res = RES_BAD_ARG;
      goto error;
    }
  }
exit:
  return res;
error:
  goto exit;
}

static INLINE void
log_err
  (const char* filename,
   const yaml_node_t* node,
   const char* fmt,
   ...)
{
  va_list vargs_list;
  ASSERT(node && fmt);

  fprintf(stderr, "%s:%lu:%lu: ",
    filename,
    (unsigned long)node->start_mark.line+1,
    (unsigned long)node->start_mark.column+1);
  va_start(vargs_list, fmt);
  vfprintf(stderr, fmt, vargs_list);
  va_end(vargs_list);
}

static res_T
parse_yaml_double
  (const char* filename,
   const yaml_node_t* node,
   const double min_val, /* Minimum valid value */
   const double max_val, /* Maximum valid value */
   double* value)
{
  res_T res = RES_OK;
  ASSERT(filename && node && value && min_val < max_val);

  if(node->type != YAML_SCALAR_NODE) {
    log_err(filename, node, "expecting a floating point number.\n");
    return RES_BAD_ARG;
  }
  res = cstr_to_double((char*)node->data.scalar.value, value);
  if(res != RES_OK) {
    log_err(filename, node, "invalid floating point number `%s'.\n",
      node->data.scalar.value);
    return RES_BAD_ARG;
  }
  if(*value < min_val || *value > max_val) {
    log_err(filename, node,
      "the floating point number %g is not in [%g, %g].\n",
      *value, min_val, max_val);
    return RES_BAD_ARG;
  }
  return RES_OK;
}

static res_T
parse_yaml_uint
  (const char* filename,
   const yaml_node_t* node,
   const unsigned min_val, /* Minimum valid value */
   const unsigned max_val, /* Maximum valid value */
   unsigned* value)
{
  res_T res = RES_OK;
  ASSERT(filename && node && value);
  ASSERT(min_val < max_val);

  if(node->type != YAML_SCALAR_NODE) {
    log_err(filename, node, "expecting an unsigned integer.\n");
    return RES_BAD_ARG;
  }
  res = cstr_to_uint((char*)node->data.scalar.value, value);
  if(res != RES_OK) {
    log_err(filename, node, "invalid unsigned integer `%s'.\n",
      node->data.scalar.value);
    return RES_BAD_ARG;
  }
  if(*value < min_val || *value > max_val) {
    log_err(filename, node,
      "the unsigned integer %u is not in [%u, %u].\n",
      *value, min_val, max_val);
    return RES_BAD_ARG;
  }

  return RES_OK;
}

static res_T
parse_yaml_param_mu_sigma
  (const char* filename,
   yaml_document_t* doc,
   const yaml_node_t* distrib,
   const char* distrib_name,
   const double min_val, /* Minimum valid value fo the parameter */
   const double max_val, /* Maximum valid value of the parameter */
   double* mu,
   double* sigma)
{
  enum {
    MU = BIT(0),
    SIGMA = BIT(1)
  };
  size_t nattrs;
  size_t i;
  int mask = 0; /* Register the parsed attributes */
  res_T res = RES_OK;
  ASSERT(filename && doc && distrib && distrib_name);
  ASSERT(min_val < max_val && mu && sigma);

  if(distrib->type != YAML_MAPPING_NODE) {
    log_err(filename, distrib,
      "expecting a mapping of the %s attributes.\n", distrib_name);
    return RES_BAD_ARG;
  }

  /* Parse the log normal attributes */
  nattrs = (size_t)
    (distrib->data.mapping.pairs.top - distrib->data.mapping.pairs.start);
  FOR_EACH(i, 0, nattrs) {
    yaml_node_t* key, *val;

    key=yaml_document_get_node(doc, distrib->data.mapping.pairs.start[i].key);
    val=yaml_document_get_node(doc, distrib->data.mapping.pairs.start[i].value);
    ASSERT(key->type == YAML_SCALAR_NODE);

    #define SETUP_MASK(Flag, Name) {                                           \
      if(mask & Flag) {                                                        \
        log_err(filename, key, "the "Name" %s attribute is already defined.\n",\
          distrib_name);                                                       \
        return RES_BAD_ARG;                                                    \
      }                                                                        \
      mask |= Flag;                                                            \
    } (void)0

    /* mu attribute */
    if(!strcmp((char*)key->data.scalar.value, "mu")) {
      SETUP_MASK(MU, "`mu'");
      res = parse_yaml_double(filename, val, min_val, max_val, mu);

    /* sigma attribute */
    } else if(!strcmp((char*)key->data.scalar.value, "sigma")) {
      SETUP_MASK(SIGMA, "`sigma'");
      res = parse_yaml_double(filename, val, DBL_MIN, DBL_MAX, sigma);

    /* unknown attribute */
    } else {
      log_err(filename, key, "unknown %s attribute `%s'.\n",
        distrib_name, key->data.scalar.value);
      return RES_BAD_ARG;
    }
    if(res != RES_OK) return res;

    #undef SETUP_MASK
  }

  /* Ensure that the attributes are all parsed */
  if(!(mask & MU)) {
    log_err(filename, distrib, "missing the `mu' %s attribute.\n",
      distrib_name);
    return RES_BAD_ARG;
  } else if(!(mask & SIGMA)) {
    log_err(filename, distrib, "missing the `sigma' %s attribute.\n",
      distrib_name);
    return RES_BAD_ARG;
  }

  return RES_OK;
}

static res_T
parse_yaml_param_histogram
  (const char* filename,
   yaml_document_t* doc,
   const yaml_node_t* histo,
   const double min_val, /* Minimum valid value fo the parameter */
   const double max_val, /* Maximum valid value of the parameter */
   struct schiff_param* param)
{
  enum {
    LOWER = BIT(0),
    UPPER = BIT(1),
    PROBAS = BIT(2)
  };
  size_t ientry, nentries, nattrs;
  size_t i;
  int mask = 0; /* Register the parsed histogram attributes */
  double accum_proba;
  res_T res = RES_OK;
  ASSERT(filename && doc && histo && param && min_val < max_val);

  param->distribution = SCHIFF_PARAM_HISTOGRAM;
  param->data.histogram.entries = NULL;

  if(histo->type != YAML_MAPPING_NODE) {
    log_err(filename, histo, "expecting a mapping of the histogram data.\n");
    res = RES_BAD_ARG;
    goto error;
  }

  /* Parse the histogram data */
  nattrs = (size_t)
    (histo->data.mapping.pairs.top - histo->data.mapping.pairs.start);
  FOR_EACH(i, 0, nattrs) {
    yaml_node_t *key, *val;

    key = yaml_document_get_node(doc, histo->data.mapping.pairs.start[i].key);
    val = yaml_document_get_node(doc, histo->data.mapping.pairs.start[i].value);
    ASSERT(key->type == YAML_SCALAR_NODE);

    #define SETUP_MASK(Flag, Name) {                                           \
      if(mask & Flag) {                                                        \
        log_err(filename, key, "the "Name" is already defined.\n");            \
        return RES_BAD_ARG;                                                    \
      }                                                                        \
      mask |= Flag;                                                            \
    } (void)0

    /* Histogram lower bound */
    if(!strcmp((char*)key->data.scalar.value, "lower")) {
      SETUP_MASK(LOWER, "histogram lower bound");
      res = parse_yaml_double
        (filename, val, min_val, max_val, &param->data.histogram.lower);
      if(res != RES_OK) goto error;

    /* Histogram upper bound */
    } else if(!strcmp((char*)key->data.scalar.value, "upper")) {
      SETUP_MASK(UPPER, "histogram upper bound");
      res = parse_yaml_double
        (filename, val, min_val, max_val, &param->data.histogram.upper);
      if(res != RES_OK) goto error;

    /* Histogram entries */
    } else if(!strcmp((char*)key->data.scalar.value, "probabilities")) {
      SETUP_MASK(PROBAS, "histogram data");

      if(val->type != YAML_SEQUENCE_NODE) {
        log_err(filename, val,
          "expecting a sequence of floating point numbers.\n");
        res = RES_BAD_ARG;
        goto error;
      }

      nentries = (size_t)
        (val->data.sequence.items.top - val->data.sequence.items.start);
      if(!sa_add(param->data.histogram.entries, nentries)) {
        log_err(filename, val,
          "couldn't allocate an histogram with %lu entries.\n",
          (unsigned long)nentries);
        res = RES_MEM_ERR;
        goto error;
      }

      /* Parse histogram entries */
      accum_proba = 0;
      FOR_EACH(ientry, 0, nentries) {
        double proba;
        yaml_node_t* node;
        node = yaml_document_get_node(doc, val->data.sequence.items.start[ientry]);

        res = parse_yaml_double(filename, node, DBL_MIN, DBL_MAX, &proba);
        if(res != RES_OK) goto error;

        accum_proba += proba;
        param->data.histogram.entries[ientry] = accum_proba;
      }

      /* Normalize the histogram entries */
      FOR_EACH(ientry, 0, nentries)
        param->data.histogram.entries[ientry] /= accum_proba;

    } else {
      log_err(filename, key, "unknown histogram data `%s'.\n",
        key->data.scalar.value);
      res = RES_BAD_ARG;
      goto error;
    }

    #undef SETUP_MASK
  }

  /* Ensure that the histogram data are all parsed. */
  if(!(mask & LOWER)) {
    log_err(filename, histo, "missing the histogram lower parameter.\n");
    res = RES_BAD_ARG;
    goto error;
  } else if(!(mask & UPPER)) {
    log_err(filename, histo, "missing the histogram upper parameter.\n");
    res = RES_BAD_ARG;
    goto error;
  } else if(!(mask & PROBAS)) {
    log_err(filename, histo, "missing the histogram probabilities.\n");
    res = RES_BAD_ARG;
    goto error;
  }

  /*  Check the histogram interval */
  if(param->data.histogram.upper <= param->data.histogram.lower) {
    log_err(filename, histo, "invalid histogram interval [%g, %g].\n",
      param->data.histogram.lower, param->data.histogram.upper);
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  return res;
error:
  if(param->data.histogram.entries) {
    sa_release(param->data.histogram.entries);
    param->data.histogram.entries = NULL;
  }
  goto exit;
}

static res_T
parse_yaml_param_distribution
  (const char* filename,
   yaml_document_t* doc,
   const yaml_node_t* node,
   const double min_val, /* Minimum valid value fo the parameter */
   const double max_val, /* Maximum valid value of the parameter */
   struct schiff_param* param)
{
  res_T res = RES_OK;
  ASSERT(filename && doc && node && param && min_val < max_val);

  if(node->type == YAML_SCALAR_NODE) { /* Floating point constant */
    param->distribution = SCHIFF_PARAM_CONSTANT;
    res = parse_yaml_double
      (filename, node, min_val, max_val, &param->data.constant);
    if(res != RES_OK) return res;

  } else if(node->type == YAML_MAPPING_NODE) {
    yaml_node_t* key, *val;

    if(node->data.mapping.pairs.top - node->data.mapping.pairs.start != 1) {
      log_err(filename, node,
      "expecting a mapping from a parameter distribution to its attributes.\n");
      return RES_BAD_ARG;
    }

    key = yaml_document_get_node(doc, node->data.mapping.pairs.start[0].key);
    val = yaml_document_get_node(doc, node->data.mapping.pairs.start[0].value);
    ASSERT(key->type == YAML_SCALAR_NODE);

    /* Lognormal distribution */
    if(!strcmp((char*)key->data.scalar.value, "lognormal")) {
      res = parse_yaml_param_mu_sigma(filename, doc, val, "lognormal", min_val,
        max_val, &param->data.lognormal.mu, &param->data.lognormal.sigma);
      if(res != RES_OK) return res;
      param->distribution = SCHIFF_PARAM_LOGNORMAL;

    /* Gaussian distribution */
    } else if(!strcmp((char*)key->data.scalar.value, "gaussian")) {
      res = parse_yaml_param_mu_sigma(filename, doc, val, "gaussian", min_val,
        max_val, &param->data.gaussian.mu, &param->data.gaussian.sigma);
      if(res != RES_OK) return res;
      param->distribution = SCHIFF_PARAM_GAUSSIAN;
      param->data.gaussian.range[0] = min_val;
      param->data.gaussian.range[1] = max_val;

    /* Histogram distribution */
    } else if(!strcmp((char*)key->data.scalar.value, "histogram")) {
      res = parse_yaml_param_histogram
        (filename, doc, val, min_val, max_val, param);
      if(res != RES_OK) return res;

    /* Unknown distribution */
    } else {
      log_err(filename, key, "unknown parameter distribution `%s'.\n",
        key->data.scalar.value);
      return RES_BAD_ARG;
    }

  } else {
    log_err(filename, node, "unexpected YAML node.\n");
    return RES_BAD_ARG;
  }

  return RES_OK;
}

static res_T
parse_yaml_superformula
  (const char* filename,
   yaml_document_t* doc,
   const yaml_node_t* node,
   struct schiff_param formula[6])
{
  int mask = 0; /* Register the parsed histogram attributes */
  size_t nattrs;
  size_t i;
  res_T res = RES_OK;
  ASSERT(filename && doc && node && formula);

  if(node->type != YAML_MAPPING_NODE) {
    log_err(filename, node,
      "expecting a mapping of superformula parameters.\n");
    return RES_BAD_ARG;
  }

  nattrs = (size_t)
    (node->data.mapping.pairs.top - node->data.mapping.pairs.start);

  FOR_EACH(i, 0, nattrs) {
    yaml_node_t* key, *val;

    key = yaml_document_get_node(doc, node->data.mapping.pairs.start[i].key);
    val = yaml_document_get_node(doc, node->data.mapping.pairs.start[i].value);
    ASSERT(key->type == YAML_SCALAR_NODE);

    #define PARSE_SUPER_SHAPE_PARAM(Param)                                     \
      if(!strcmp((char*)key->data.scalar.value, STR(Param))) {                 \
        if(mask & BIT(Param)) {                                                \
          log_err(filename, key,                                               \
            "the "STR(Param)" superformula parameter is already defined.\n");  \
          return RES_BAD_ARG;                                                  \
        }                                                                      \
        mask |= BIT(Param);                                                    \
        res = parse_yaml_param_distribution                                    \
          (filename, doc, val, DBL_MIN, DBL_MAX, formula + Param);             \
        if(res != RES_OK) return res;                                          \
        continue;                                                              \
      } (void)0
    PARSE_SUPER_SHAPE_PARAM(A);
    PARSE_SUPER_SHAPE_PARAM(B);
    PARSE_SUPER_SHAPE_PARAM(M);
    PARSE_SUPER_SHAPE_PARAM(N0);
    PARSE_SUPER_SHAPE_PARAM(N1);
    PARSE_SUPER_SHAPE_PARAM(N2);
    #undef PARSE_SUPER_SHAPE_PARAM

    log_err(filename, key, "unknown superformula parameter `%s'.\n",
      key->data.scalar.value);
    return RES_BAD_ARG;
  }
  #define CHECK_SUPER_SHAPE_PARAM(Param)                                       \
    if(!(mask & BIT(Param))) {                                                 \
      log_err(filename, node,                                                  \
        "missing the "STR(Param)" superformula parameter.\n");                 \
      return RES_BAD_ARG;                                                      \
    } (void)0
  CHECK_SUPER_SHAPE_PARAM(A);
  CHECK_SUPER_SHAPE_PARAM(B);
  CHECK_SUPER_SHAPE_PARAM(M);
  CHECK_SUPER_SHAPE_PARAM(N0);
  CHECK_SUPER_SHAPE_PARAM(N1);
  CHECK_SUPER_SHAPE_PARAM(N2);
  #undef CHECK_SUPER_SHAPE_PARAM
  return RES_OK;
}

static res_T
parse_yaml_ellipsoid
  (const char* filename,
   yaml_document_t* doc,
   const yaml_node_t* node,
   struct schiff_geometry* geom,
   double* geom_proba)
{
  enum {
    PROBA = BIT(0),
    LENGTH_a = BIT(1),
    LENGTH_c = BIT(2),
    RADIUS_SPHERE = BIT(3),
    SLICES = BIT(4)
  };
  int mask = 0; /* Register the parsed histogram attributes */
  size_t nattrs;
  size_t i;
  res_T res = RES_OK;
  ASSERT(filename && doc && node && geom && geom_proba);

/* Setup the type at the beginning in order to define what arguments should
   * be released if a parsing error occurs. Note that one can define the main
   * type or its "equivalent sphere" variation. */
  geom->type = SCHIFF_ELLIPSOID;

  if(node->type != YAML_MAPPING_NODE) {
    log_err(filename, node, "expecting a mapping of ellipsoid attributes.\n");
    return RES_BAD_ARG;
  }

  nattrs = (size_t)
    (node->data.mapping.pairs.top - node->data.mapping.pairs.start);

  FOR_EACH(i, 0, nattrs) {
    yaml_node_t* key;
    yaml_node_t* val;

    key = yaml_document_get_node(doc, node->data.mapping.pairs.start[i].key);
    val = yaml_document_get_node(doc, node->data.mapping.pairs.start[i].value);
    ASSERT(key->type == YAML_SCALAR_NODE);

    #define SETUP_MASK(Flag, Name) {                                           \
      if(mask & Flag) {                                                        \
        log_err(filename, key, "the "Name" is already defined.\n");            \
        return RES_BAD_ARG;                                                    \
      }                                                                        \
      mask |= Flag;                                                            \
    } (void)0

    /* Probability of the distribution */
    if(!strcmp((char*)key->data.scalar.value, "proba")) {
      SETUP_MASK(PROBA, "sphere proba");
      res = parse_yaml_double(filename, val, DBL_MIN, DBL_MAX, geom_proba);

    /* # slices used to discretized the triangular ellipsoid */
    } else if(!strcmp((char*)key->data.scalar.value, "slices")) {
      SETUP_MASK(SLICES, "ellipsoid number of slices");
      res = parse_yaml_uint
        (filename, val, 4, 32768, &geom->data.ellipsoid.nslices);

    /* equivalent sphere radius */
    } else if(!strcmp((char*)key->data.scalar.value, "radius_sphere")) {
      SETUP_MASK(RADIUS_SPHERE, "equivalent sphere radius of the ellipsoid");
      res = parse_yaml_param_distribution(filename, doc, val, DBL_MIN, DBL_MAX,
        &geom->data.ellipsoid.radius_sphere);

    /* Length of the ellipsoid "a" semi-principal axis */
    } else if(!strcmp((char*)key->data.scalar.value, "a")) {
      SETUP_MASK(LENGTH_a, "ellipsoid \"a\" parameter");
      res = parse_yaml_param_distribution
        (filename, doc, val, DBL_MIN, DBL_MAX, &geom->data.ellipsoid.a);

    /* Length of the ellipsoid "c" semi-principal axis */
    } else if(!strcmp((char*)key->data.scalar.value, "c")) {
      SETUP_MASK(LENGTH_c, "ellipsoid \"c\" parameter");
      res = parse_yaml_param_distribution
        (filename, doc, val, DBL_MIN, DBL_MAX, &geom->data.ellipsoid.c);

    /* Error */
    } else {
      log_err(filename, key, "unknown sphere attribute `%s'.\n",
        key->data.scalar.value);
      return RES_BAD_ARG;
    }
    if(res != RES_OK) return res;

    #undef SETUP_MASK
  }

  /* Ensure the completeness of the ellipsoid distribution */
  if(!(mask & LENGTH_a)) {
    log_err(filename, node,
      "missing the length of the semi principal axis \"a\".\n");
    return RES_BAD_ARG;
  } else if(!(mask & LENGTH_c)) {
    log_err(filename, node,
      "missing the length of the semi principal axis \"c\".\n");
    return RES_BAD_ARG;
  }
  /* Setup the default values if required */
  if(!(mask & PROBA)) { /* Default proba */
    *geom_proba = 1.0;
  }
  if(!(mask & SLICES)) { /* Default number of slices */
    geom->data.ellipsoid.nslices = SCHIFF_ELLIPSOID_DEFAULT.nslices;
  }
  /* Define the geometry type */
  if(!(mask & RADIUS_SPHERE)) {
    geom->type = SCHIFF_ELLIPSOID;
  } else {
    if(geom->data.ellipsoid.a.distribution != SCHIFF_PARAM_CONSTANT
    || geom->data.ellipsoid.c.distribution != SCHIFF_PARAM_CONSTANT) {
      log_err(filename, node,
        "the radius_sphere parameter cannot be defined with non constant "
        "ellipsoid attributes.\n");
      return RES_BAD_ARG;
    }
    geom->type = SCHIFF_ELLIPSOID_AS_SPHERE;
  }
  return RES_OK;
}

static res_T
parse_yaml_cylinder
  (const char* filename,
   yaml_document_t* doc,
   const yaml_node_t* node,
   struct schiff_geometry* geom,
   double* geom_proba)
{
  enum {
    PROBA = BIT(0),
    RADIUS = BIT(1),
    HEIGHT = BIT(2),
    RADIUS_SPHERE = BIT(3),
    SLICES = BIT(4)
  };
  int mask = 0; /* Register the parsed attributes */
  size_t nattrs;
  size_t i;
  res_T res = RES_OK;
  ASSERT(filename && doc && node && geom && geom_proba);

  /* Setup the type at the beginning in order to define what arguments should
   * be released if a parsing error occurs. Note that one can define the main
   * type or its "equivalent sphere" variation. */
  geom->type = SCHIFF_CYLINDER;

  if(node->type != YAML_MAPPING_NODE) {
    log_err(filename, node, "expecting a mapping of cylinder attributes.\n");
    return RES_BAD_ARG;
  }

  nattrs = (size_t)
    (node->data.mapping.pairs.top - node->data.mapping.pairs.start);

  FOR_EACH(i, 0, nattrs) {
    yaml_node_t* key;
    yaml_node_t* val;

    key = yaml_document_get_node(doc, node->data.mapping.pairs.start[i].key);
    val = yaml_document_get_node(doc, node->data.mapping.pairs.start[i].value);
    ASSERT(key->type == YAML_SCALAR_NODE);

    #define SETUP_MASK(Flag, Name) {                                           \
      if(mask & Flag) {                                                        \
        log_err(filename, key, "the "Name" is already defined.\n");            \
        return RES_BAD_ARG;                                                    \
      }                                                                        \
      mask |= Flag;                                                            \
    } (void)0

    /* Distribution  probability */
    if(!strcmp((char*)key->data.scalar.value, "proba")) {
      SETUP_MASK(PROBA, "cylinder proba");
      res = parse_yaml_double(filename, val, DBL_MIN, DBL_MAX, geom_proba);

    /* Cylinder radius */
    } else if(!strcmp((char*)key->data.scalar.value, "radius")) {
      SETUP_MASK(RADIUS, "cylinder radius");
      res = parse_yaml_param_distribution
        (filename, doc, val, DBL_MIN, DBL_MAX, &geom->data.cylinder.radius);

    /* Cylinder height */
    } else if(!strcmp((char*)key->data.scalar.value, "height")) {
      SETUP_MASK(HEIGHT, "cylinder height");
      res = parse_yaml_param_distribution
        (filename, doc, val, DBL_MIN, DBL_MAX, &geom->data.cylinder.height);

    /* Equivalent sphere radius */
    } else if(!strcmp((char*)key->data.scalar.value, "radius_sphere")) {
      SETUP_MASK(RADIUS_SPHERE, "equivalent sphere radius of the cylinder");
      res = parse_yaml_param_distribution(filename, doc, val, DBL_MIN, DBL_MAX,
        &geom->data.cylinder.radius_sphere);

    /* # slices used to discretized the cylinder */
    } else if(!strcmp((char*)key->data.scalar.value, "slices")) {
      SETUP_MASK(SLICES, "cylinder number of slices");
      res = parse_yaml_uint
        (filename, val, 4, 32768, &geom->data.cylinder.nslices);

    /* Error */
    } else {
      log_err(filename, key, "unknown cylinder attribute `%s'.\n",
        key->data.scalar.value);
      res = RES_BAD_ARG;
    }
    if(res != RES_OK) return res;

    #undef SETUP_MASK
  }

  /* Ensure the completness of the cylinder distribution */
  if(!(mask & RADIUS)) {
    log_err(filename, node, "missing the radius attribute.\n");
    return RES_BAD_ARG;
  } else if(!(mask & HEIGHT)) {
    log_err(filename, node, "missing the height attribute.\n");
    return RES_BAD_ARG;
  }
  /* Setup the default values if required */
  if(!(mask & PROBA)) { /* Default proba */
    *geom_proba = 1.0;
  }
  if(!(mask & SLICES)) { /* Default number of slices */
    geom->data.cylinder.nslices = SCHIFF_CYLINDER_DEFAULT.nslices;
  }
  /* Define the geometry type */
  if(!(mask & RADIUS_SPHERE)) {
    geom->type = SCHIFF_CYLINDER;
  } else {
    if(geom->data.cylinder.radius.distribution != SCHIFF_PARAM_CONSTANT
    || geom->data.cylinder.height.distribution != SCHIFF_PARAM_CONSTANT) {
      log_err(filename, node,
        "the radius_sphere parameter cannot be defined with non constant "
        "cylinder attributes.\n");
      return RES_BAD_ARG;
    }
    geom->type = SCHIFF_CYLINDER_AS_SPHERE;
  }
  return RES_OK;
}

static res_T
parse_yaml_helical_pipe
  (const char* filename,
   yaml_document_t* doc,
   const yaml_node_t* node,
   struct schiff_geometry* geom,
   double* geom_proba)
{
  enum {
    PROBA = BIT(0),
    PITCH = BIT(1),
    HEIGHT = BIT(2),
    RADIUS_HELICOID = BIT(3),
    RADIUS_CIRCLE = BIT(4),
    SLICES_HELICOID = BIT(5),
    SLICES_CIRCLE = BIT(6),
    RADIUS_SPHERE = BIT(7)
  };
  int mask = 0; /* Register the parsed attributes */
  size_t nattrs;
  size_t i;
  res_T res = RES_OK;
  ASSERT(filename && doc && node && geom && geom_proba);

  /* Setup the type at the beginning in order to define what arguments should
   * be released if a parsing error occurs. Note that one can define the main
   * type or its "equivalent sphere" variation. */
  geom->type = SCHIFF_HELICAL_PIPE;

  if(node->type != YAML_MAPPING_NODE) {
    log_err(filename, node, "expecting a mapping of helical pipe attributes.\n");
    return RES_BAD_ARG;
  }

  nattrs = (size_t)
    (node->data.mapping.pairs.top - node->data.mapping.pairs.start);

  FOR_EACH(i, 0, nattrs) {
    yaml_node_t* key;
    yaml_node_t* val;

    key = yaml_document_get_node(doc, node->data.mapping.pairs.start[i].key);
    val = yaml_document_get_node(doc, node->data.mapping.pairs.start[i].value);
    ASSERT(key->type == YAML_SCALAR_NODE);

    #define SETUP_MASK(Flag, Name) {                                           \
      if(mask & Flag) {                                                        \
        log_err(filename, key, "the "Name" is already defined.\n");            \
        return RES_BAD_ARG;                                                    \
      }                                                                        \
      mask |= Flag;                                                            \
    } (void)0

    /* Probability of the distribution */
    if(!strcmp((char*)key->data.scalar.value, "proba")) {
      SETUP_MASK(PROBA, "helical pipe  proba");
      res = parse_yaml_double(filename, val, DBL_MIN, DBL_MAX, geom_proba);

    /* Helicoid pitch */
    } else if(!strcmp((char*)key->data.scalar.value, "pitch")) {
      SETUP_MASK(PITCH, "helical pipe pitch");
      res = parse_yaml_param_distribution
        (filename, doc, val, DBL_MIN, DBL_MAX, &geom->data.helical_pipe.pitch);

    /* Helicoid height */
    } else if(!strcmp((char*)key->data.scalar.value, "height")) {
      SETUP_MASK(HEIGHT, "helical pipe height");
      res = parse_yaml_param_distribution
        (filename, doc, val, DBL_MIN, DBL_MAX, &geom->data.helical_pipe.height);

    /* Radius of the helicoid */
    } else if(!strcmp((char*)key->data.scalar.value, "radius_helicoid")) {
      SETUP_MASK(RADIUS_HELICOID, "helicoid radius");
      res = parse_yaml_param_distribution(filename, doc, val, DBL_MIN, DBL_MAX,
        &geom->data.helical_pipe.radius_helicoid);

    /* Radius of the meridian circle */
    } else if(!strcmp((char*)key->data.scalar.value, "radius_circle")) {
      SETUP_MASK(RADIUS_CIRCLE, "circle radius of the helical pipe");
      res = parse_yaml_param_distribution(filename, doc, val, DBL_MIN, DBL_MAX,
        &geom->data.helical_pipe.radius_circle);

    /* # slices of the helicoid */
    } else if(!strcmp((char*)key->data.scalar.value, "slices_helicoid")) {
      SETUP_MASK(SLICES_HELICOID, "helicoid number of slices");
      res = parse_yaml_uint
        (filename, val, 4, 32768, &geom->data.helical_pipe.nslices_helicoid);

    /* # slices of the circle */
    } else if(!strcmp((char*)key->data.scalar.value, "slices_circle")) {
      SETUP_MASK(SLICES_CIRCLE, "helicoid meridian circle number of slices");
      res = parse_yaml_uint
        (filename, val, 4, 32768, &geom->data.helical_pipe.nslices_circle);

    /* Equivalent sphere radius */
    } else if(!strcmp((char*)key->data.scalar.value, "radius_sphere")) {
      SETUP_MASK(RADIUS_SPHERE, "equivalent sphere radius of the helical pipe");
      res = parse_yaml_param_distribution(filename, doc, val, DBL_MIN, DBL_MAX,
        &geom->data.helical_pipe.radius_sphere);

    /* Error */
    } else {
      log_err(filename, key, "unknown helical pipe attribute `%s'.\n",
        key->data.scalar.value);
      return RES_BAD_ARG;
    }
    if(res != RES_OK) return res;
    #undef SETUP_MASK
  }

  /* Ensure the completeness of the helical pipe distribution */
  if(!(mask & PITCH)) {
    log_err(filename, node, "missing the pitch of the helical pipe.\n");
    return RES_BAD_ARG;
  } else if(!(mask & HEIGHT)) {
    log_err(filename, node, "missing the height of the helical pipe.\n");
    return RES_BAD_ARG;
  } else if(!(mask & RADIUS_HELICOID)) {
    log_err(filename, node, "missing the radius of the helicoid.\n");
    return RES_BAD_ARG;
  } else if(!(mask & RADIUS_CIRCLE)) {
    log_err(filename, node, "missing the radius of the meridian circle.\n");
    return RES_BAD_ARG;
  }
  /* Setup the default values if required */
  if(!(mask & PROBA)) {
    *geom_proba = 1.0;
  }
  if(!(mask & SLICES_HELICOID)) {
    geom->data.helical_pipe.nslices_helicoid =
      SCHIFF_HELICAL_PIPE_DEFAULT.nslices_helicoid;
  }
  if(!(mask & SLICES_CIRCLE)) {
    geom->data.helical_pipe.nslices_circle =
      SCHIFF_HELICAL_PIPE_DEFAULT.nslices_circle;
  }
  /* Define the geometry type */
  if(!(mask & RADIUS_SPHERE)) {
    geom->type = SCHIFF_HELICAL_PIPE;
  } else {
    const struct schiff_helical_pipe* hpipe = &geom->data.helical_pipe;
    if(hpipe->pitch.distribution != SCHIFF_PARAM_CONSTANT
    || hpipe->height.distribution != SCHIFF_PARAM_CONSTANT
    || hpipe->radius_helicoid.distribution != SCHIFF_PARAM_CONSTANT
    || hpipe->radius_circle.distribution != SCHIFF_PARAM_CONSTANT) {
      log_err(filename, node,
        "the radius_sphere parameter cannot be defined with non constant "
        "helical pipe attributes.\n");
      return RES_BAD_ARG;
    }
    geom->type = SCHIFF_HELICAL_PIPE_AS_SPHERE;
  }
  return RES_OK;
}

static res_T
parse_yaml_sphere
  (const char* filename,
   yaml_document_t* doc,
   const yaml_node_t* node,
   struct schiff_geometry* geom,
   double* geom_proba)
{
  enum {
    PROBA = BIT(0),
    RADIUS = BIT(1),
    SLICES = BIT(2)
  };
  int mask = 0; /* Register the parsed attributes */
  size_t nattrs;
  size_t i;
  res_T res = RES_OK;
  ASSERT(filename && doc && node && geom && geom_proba);

  /* Setup the type at the beginning in order to define what arguments should
   * be released if a parsing error occurs. */
  geom->type = SCHIFF_SPHERE;

  if(node->type != YAML_MAPPING_NODE) {
    log_err(filename, node, "expecting a mapping of sphere attributes.\n");
    return RES_BAD_ARG;
  }

  nattrs = (size_t)
    (node->data.mapping.pairs.top - node->data.mapping.pairs.start);

  FOR_EACH(i, 0, nattrs) {
    yaml_node_t* key;
    yaml_node_t* val;

    key = yaml_document_get_node(doc, node->data.mapping.pairs.start[i].key);
    val = yaml_document_get_node(doc, node->data.mapping.pairs.start[i].value);
    ASSERT(key->type == YAML_SCALAR_NODE);

    #define SETUP_MASK(Flag, Name) {                                           \
      if(mask & Flag) {                                                        \
        log_err(filename, key, "the "Name" is already defined.\n");            \
        return RES_BAD_ARG;                                                    \
      }                                                                        \
      mask |= Flag;                                                            \
    } (void)0

    /* Probality to sample this geometry */
    if(!strcmp((char*)key->data.scalar.value, "proba")) {
      SETUP_MASK(PROBA, "sphere proba");
      res = parse_yaml_double(filename, val, DBL_MIN, DBL_MAX, geom_proba);

    /* Sphere radius */
    } else if(!strcmp((char*)key->data.scalar.value, "radius")) {
      SETUP_MASK(RADIUS, "sphere radius");
      res = parse_yaml_param_distribution
        (filename, doc, val, DBL_MIN, DBL_MAX, &geom->data.sphere.radius);

    /* # slices used to discretized the triangular sphere */
    } else if(!strcmp((char*)key->data.scalar.value, "slices")) {
      SETUP_MASK(SLICES, "sphere number of slices");
      res = parse_yaml_uint
        (filename, val, 4, 32768, &geom->data.sphere.nslices);

    /* Error */
    } else {
      log_err(filename, key, "unkown sphere attribute `%s'.\n",
        key->data.scalar.value);
      return RES_BAD_ARG;
    }
    if(res != RES_OK) return res;

    #undef SETUP_MASK
  }

  /* Ensure the completness of the spherical distribution */
  if(!(mask & RADIUS)) {
    log_err(filename, node, "missing the radius attribute.\n");
    return RES_BAD_ARG;
  }
  if(!(mask & PROBA)) { /* Default proba */
    *geom_proba = 1.0;
  }
  if(!(mask & SLICES)) { /* Default number of slices */
    geom->data.sphere.nslices = SCHIFF_SPHERE_DEFAULT.nslices;
  }
  return RES_OK;
}

static res_T
parse_yaml_supershape
  (const char* filename,
   yaml_document_t* doc,
   const yaml_node_t* node,
   struct schiff_geometry* geom,
   double* geom_proba)
{
  enum {
    FORMULA0 = BIT(0),
    FORMULA1 = BIT(1),
    RADIUS_SPHERE = BIT(2),
    PROBA = BIT(3),
    SLICES = BIT(4)
  };
  int mask = 0; /* Register the parsed attributes */
  size_t nattrs;
  size_t i;
  res_T res = RES_OK;
  ASSERT(filename && doc && node && geom && geom_proba);

  /* Setup the type at the beginning in order to define what arguments should
   * be released if a parsing error occurs. Note that one can define the main
   * type or its "equivalent sphere" variation. */
  geom->type = SCHIFF_SUPERSHAPE;

  if(node->type != YAML_MAPPING_NODE) {
    log_err(filename, node, "expecting a mapping of supershape attributes.\n");
    return RES_BAD_ARG;
  }

  nattrs = (size_t)
    (node->data.mapping.pairs.top - node->data.mapping.pairs.start);

  FOR_EACH(i, 0, nattrs) {
    yaml_node_t* key;
    yaml_node_t* val;

    key = yaml_document_get_node(doc, node->data.mapping.pairs.start[i].key);
    val = yaml_document_get_node(doc, node->data.mapping.pairs.start[i].value);
    ASSERT(key->type == YAML_SCALAR_NODE);

    #define SETUP_MASK(Flag, Name) {                                           \
      if(mask & Flag) {                                                        \
        log_err(filename, key, "the "Name" is already defined.\n");            \
        return RES_BAD_ARG;                                                    \
      }                                                                        \
      mask |= Flag;                                                            \
    } (void)0

    /* Geometry probability */
    if(!strcmp((char*)key->data.scalar.value, "proba")) {
      SETUP_MASK(PROBA, "supershape proba");
      res = parse_yaml_double(filename, val, DBL_MIN, DBL_MAX, geom_proba);

    /* Super shape formula0 */
    } else if(!strcmp((char*)key->data.scalar.value, "formula0")) {
      SETUP_MASK(FORMULA0, "supershape formula0");
      res = parse_yaml_superformula
        (filename, doc, val, geom->data.supershape.formulas[0]);

    /* Super shape formula1 */
    } else if(!strcmp((char*)key->data.scalar.value, "formula1")) {
      SETUP_MASK(FORMULA1, "supershape formula1");
      res = parse_yaml_superformula
        (filename, doc, val, geom->data.supershape.formulas[1]);

    /* Equivalent sphere radius */
    } else if(!strcmp((char*)key->data.scalar.value, "radius_sphere")) {
      SETUP_MASK(RADIUS_SPHERE, "equivalent sphere radius of the supershape");
      res = parse_yaml_param_distribution(filename, doc, val, DBL_MIN, DBL_MAX,
        &geom->data.supershape.radius_sphere);

    /* # slices used to discretized the super shape */
    } else if(!strcmp((char*)key->data.scalar.value, "slices")) {
      SETUP_MASK(SLICES, "supershape number of slices");
      res = parse_yaml_uint
        (filename, val, 4, 32768, &geom->data.supershape.nslices);

    /* Error */
    } else {
      log_err(filename, key, "unknown supershape attribute `%s'.\n",
        key->data.scalar.value);
      res = RES_BAD_ARG;
    }
    if(res != RES_OK) return res;

    #undef SETUP_MASK
  }

  /* Ensure the completness of the cylinder distribution */
  if(!(mask & FORMULA0)) {
    log_err(filename, node, "missing the formula0 attribute.\n");
    return RES_BAD_ARG;
  } else if(!(mask & FORMULA1)) {
    log_err(filename, node, "missing the formula1 attribute.\n");
    return RES_BAD_ARG;
  }
  /* Setup the default values if required */
  if(!(mask & PROBA)) { /* Default proba */
    *geom_proba = 1.0;
  }
  if(!(mask & SLICES)) { /* Default number of slices */
    geom->data.supershape.nslices = SCHIFF_SUPERSHAPE_DEFAULT.nslices;
  }
  /* Define the geometry type */
  if(!(mask & RADIUS_SPHERE)) {
    geom->type = SCHIFF_SUPERSHAPE;
  } else {
    const struct schiff_supershape* sshape = &geom->data.supershape;
    FOR_EACH(i, 0, 6) {
      if(sshape->formulas[0][i].distribution != SCHIFF_PARAM_CONSTANT
      || sshape->formulas[1][i].distribution != SCHIFF_PARAM_CONSTANT) {
        log_err(filename, node,
          "the radius_sphere parameter cannot be defined with non constant "
          "supershape attributes.\n");
        return RES_BAD_ARG;
      }
    }
    geom->type = SCHIFF_SUPERSHAPE_AS_SPHERE;
  }
  return RES_OK;
}

static res_T
parse_yaml_geom_distrib
  (const char* filename,
   yaml_document_t* doc,
   yaml_node_t* node,
   struct schiff_geometry* geom,
   double* proba)
{
  res_T res;
  yaml_node_t *key, *val;
  ASSERT(filename && doc && node && geom && proba);

  if(node->type != YAML_MAPPING_NODE
  || node->data.mapping.pairs.top - node->data.mapping.pairs.start > 1) {
    log_err(filename, node,
      "expecting a mapping of the geometry distribution to its parameters\n");
    return RES_BAD_ARG;
  }

  key = yaml_document_get_node(doc, node->data.mapping.pairs.start[0].key);
  val = yaml_document_get_node(doc, node->data.mapping.pairs.start[0].value);
  ASSERT(key->type == YAML_SCALAR_NODE);

  if(!strcmp((char*)key->data.scalar.value, "ellipsoid")) {
    res = parse_yaml_ellipsoid(filename, doc, val, geom, proba);
  } else if(!strcmp((char*)key->data.scalar.value, "cylinder")) {
    res = parse_yaml_cylinder(filename, doc, val, geom, proba);
  } else if(!strcmp((char*)key->data.scalar.value, "sphere")) {
    res = parse_yaml_sphere(filename, doc, val, geom, proba);
  } else if(!strcmp((char*)key->data.scalar.value, "helical_pipe")) {
    res = parse_yaml_helical_pipe(filename, doc, val, geom, proba);
  } else if(!strcmp((char*)key->data.scalar.value, "supershape")) {
    res = parse_yaml_supershape(filename, doc, val, geom, proba);
  } else {
    log_err(filename, key, "unknown distribution `%s'.\n",
      key->data.scalar.value);
    return RES_BAD_ARG;
  }
  if(res != RES_OK) return res;
  return RES_OK;
}

static res_T
parse_yaml
  (const char* filename,
   struct schiff_geometry** out_geoms,
   struct ssp_ranst_discrete** out_ran)
{
  yaml_parser_t parser;
  yaml_document_t doc;
  yaml_node_t* root;
  size_t ndistribs = 0;
  size_t idistrib;
  struct schiff_geometry* geoms = NULL;
  double* probas = NULL;
  struct ssp_ranst_discrete* ran = NULL;
  FILE* file = NULL;
  int doc_is_init = 0;
  res_T res = RES_OK;
  ASSERT(filename && out_geoms && out_ran);

  if(!yaml_parser_initialize(&parser)) {
    fprintf(stderr, "Couldn't intialise the YAML parser.\n");
    res = RES_UNKNOWN_ERR;
    goto exit;
  }

  file = fopen(filename, "rb");
  if(!file) {
    fprintf(stderr, "Couldn't open the YAML file `%s'.\n", filename);
    res = RES_IO_ERR;
    goto error;
  }

  yaml_parser_set_input_file(&parser, file);

  if(!yaml_parser_load(&parser, &doc)) {
    fprintf(stderr, "%s:%lu:%lu: %s.\n",
      filename,
      (unsigned long)parser.problem_mark.line+1,
      (unsigned long)parser.problem_mark.column+1,
      parser.problem);
    res = RES_IO_ERR;
    goto error;
  }
  doc_is_init = 1;

  root = yaml_document_get_root_node(&doc);
  if(!root) {
    fprintf(stderr, "Unexpected empty file `%s'.\n", filename);
    res = RES_BAD_ARG;
    goto error;
  }
  if(root->type == YAML_MAPPING_NODE) {
    ndistribs = (size_t)
      (root->data.mapping.pairs.top - root->data.mapping.pairs.start);
  } else if(root->type == YAML_SEQUENCE_NODE) {
    /* Define the number of submitted distributions */
    ndistribs = (size_t)
      (root->data.sequence.items.top - root->data.sequence.items.start);
  }

  if((root->type == YAML_MAPPING_NODE && ndistribs > 1)
  || (root->type != YAML_MAPPING_NODE && root->type != YAML_SEQUENCE_NODE)) {
    fprintf(stderr,
      "Expecting either one or a list of geometry distributions.\n");
    res = RES_BAD_ARG;
    goto error;
  }

  /* Allocate the list geometry distributions */
  if(!sa_add(geoms, ndistribs)) {
    log_err(filename, root,
      "couldn't allocate up to %lu geometry distributions.\n",
      (unsigned long)ndistribs);
    res = RES_MEM_ERR;
    goto error;
  }
  FOR_EACH(idistrib, 0, ndistribs)
    geoms[idistrib] = SCHIFF_GEOMETRY_NULL;

  /* Allocate the per geometry distribution proba */
  if(!sa_add(probas, ndistribs)) {
    log_err(filename, root,
      "couldn't allocate the list of geometry distribution probabilities.\n");
    res = RES_MEM_ERR;
    goto error;
  }

  /* Create the geometry distribution random variate */
  res = ssp_ranst_discrete_create(&mem_default_allocator, &ran);
  if(res != RES_OK) {
    log_err(filename, root,
      "couldn't allocate the random variate of geometry distributions.\n");
    goto error;
  }

  /* Only one distribution */
  if(root->type == YAML_MAPPING_NODE) {
    res = parse_yaml_geom_distrib(filename, &doc, root, &geoms[0], &probas[0]);
    if(res != RES_OK) goto error;

  /* List of geometry distributions */
  } else {
    double accum_proba = 0;
    ASSERT(root->type == YAML_SEQUENCE_NODE);

    FOR_EACH(idistrib, 0, ndistribs) {
      yaml_node_t* distrib;

      distrib = yaml_document_get_node
        (&doc, root->data.sequence.items.start[idistrib]);
      res = parse_yaml_geom_distrib
        (filename, &doc, distrib, &geoms[idistrib], &probas[idistrib]);
      if(res != RES_OK) goto error;

      accum_proba += probas[idistrib];
    }
    /* Normalized the geometry distribution probabilities */
    FOR_EACH(idistrib, 0, ndistribs-1) probas[idistrib] /= accum_proba;
    probas[ndistribs-1] = 1.f; /* Handle precision issues */
  }

  /* Setup the geometry distributions random variate */
  res = ssp_ranst_discrete_setup(ran, probas, ndistribs);
  if(res != RES_OK) {
    log_err(filename, root,
      "couldn't setup the discrete geometry distributions.\n");
    goto error;
  }

exit:
  yaml_parser_delete(&parser);
  if(doc_is_init) yaml_document_delete(&doc);
  if(file) fclose(file);
  if(probas) sa_release(probas);
  *out_geoms = geoms;
  *out_ran = ran;
  return res;
error:
  if(ran) {
    SSP(ranst_discrete_ref_put(ran));
    ran = NULL;
  }
  if(geoms) {
    FOR_EACH(idistrib, 0, ndistribs) {
      geometry_release(&geoms[idistrib]);
    }
    sa_release(geoms);
    geoms = NULL;
  }
  goto exit;
}

/*******************************************************************************
 * Local function
 ******************************************************************************/
res_T
schiff_args_init
  (struct schiff_args* args,
   const int argc,
   char** argv)
{
  int quiet = 0;
  int opt;
  int i;
  res_T res = RES_OK;
  ASSERT(argc && argv && args);

  *args = SCHIFF_ARGS_NULL;

  FOR_EACH(i, 1, argc) {
    if(!strcmp(argv[i], "--version")) {
      printf("Schiff %d.%d.%d\n",
        SCHIFF_VERSION_MAJOR,
        SCHIFF_VERSION_MINOR,
        SCHIFF_VERSION_PATCH);
      goto exit;
    }
  }

  while((opt = getopt(argc, argv, "a:A:d:Dg:G:hi:l:n:o:qw:")) != -1) {
    switch(opt) {
      case 'a':
        res = cstr_to_uint(optarg, &args->nangles);
        if(res == RES_OK && args->nangles < MIN_NANGLES) {
          fprintf(stderr,
            "%s: expecting at least "STR(MIN_NANGLES)" scattering angles.\n",
            argv[0]);
          res = RES_BAD_ARG;
        }
        break;
      case 'A':
        res = cstr_to_uint(optarg, &args->nangles_inv);
        if(res == RES_OK && args->nangles_inv < MIN_NANGLES_INV) {
          fprintf(stderr,
            "%s: expecting at least "STR(MIN_NANGLES_INV)
            " inverse cumulative phase function values.\n",
            argv[0]);
          res = RES_BAD_ARG;
        }
        break;
      case 'd':
        res = cstr_to_uint(optarg, &args->ninsamps);
        if(res == RES_OK && !args->ninsamps) {
          fprintf(stderr, "%s: the number of inner samples cannot be null.\n",
            argv[0]);
          res = RES_BAD_ARG;
        }
        break;
      case 'D': args->discard_large_angles = 1; break;
      case 'g':
        res = cstr_to_uint(optarg, &args->nrealisations);
        if(res == RES_OK && !args->nrealisations) {
          fprintf(stderr, "%s: the number of realisations cannot be null.\n",
            argv[0]);
          res = RES_BAD_ARG;
        }
        break;
      case 'G': res = cstr_to_uint(optarg, &args->ngeoms_dump); break;
      case 'h':
        print_help(argv[0]);
        schiff_args_release(args);
        return RES_OK;
      case 'i':
        res = parse_yaml(optarg, &args->geoms, &args->ran_geoms);
        break;
      case 'l':
        res = cstr_to_double(optarg, &args->characteristic_length);
        if(res == RES_OK && args->characteristic_length <= 0.0)
          res = RES_BAD_ARG;
        break;
      case 'n':
        res = cstr_to_uint(optarg, &args->nthreads);
        if(res == RES_OK && args->nthreads == 0)
          res = RES_BAD_ARG;
        break;
      case 'o': args->output_filename = optarg; break;
      case 'q': quiet = 1; break;
      case 'w': res = parse_wavelengths(optarg, args); break;
      default: res = RES_BAD_ARG; break;
    }
    if(res != RES_OK) {
      if(optarg) {
        fprintf(stderr, "%s: invalid option arguments '%s' -- '%c'\n",
          argv[0], optarg, opt);
      }
      goto error;
    }
  }

  if(args->geoms == NULL) {
    fprintf(stderr,
      "%s: missing geometry distribution.\nTry '%s -h' for more informations.\n",
      argv[0], argv[0]);
    res = RES_BAD_ARG;
    goto error;
  }

  if(args->ngeoms_dump) goto exit;

  if(!args->wavelengths) {
    fprintf(stderr,
      "%s: missing wavelengths.\nTry '%s -h' for more informations.\n",
      argv[0], argv[0]);
    res = RES_BAD_ARG;
    goto error;
  }
  /* Sort the submitted wavelengths in ascending order */
  qsort(args->wavelengths, sa_size(args->wavelengths),
    sizeof(args->wavelengths[0]), cmp_double);

  if(args->characteristic_length < 0.0) {
    fprintf(stderr,
"%s: missing the characteristic length.\nTry '%s -h' for more informations\n",
      argv[0], argv[0]);
    res = RES_BAD_ARG;
    goto error;
  }

  if(optind == argc) {
    if(!quiet) {
#ifdef OS_WINDOWS
      fprintf(stderr,
        "Enter the optical properties. "
        "Type ^Z (i.e. CTRL+z) and return to stop:\n");
#else
      fprintf(stderr,
        "Enter the optical properties. Type ^D (i.e. CTRL+d) to stop:\n");
#endif
    }
    res = schiff_optical_properties_load_stream(&args->properties, stdin, "stdin");
  } else {
    res = schiff_optical_properties_load(&args->properties, argv[optind]);
  }
  if(res != RES_OK) goto error;

  if(!args->properties) {
    fprintf(stderr,
      "%s: missing optical properties.\nTry '%s -h' for more information.\n",
      argv[0], argv[0]);
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  return res;
error:
  schiff_args_release(args);
  *args = SCHIFF_ARGS_NULL;
  goto exit;
}

void
schiff_args_release(struct schiff_args* args)
{
  size_t i, count;
  ASSERT(args);
  sa_release(args->properties);
  sa_release(args->wavelengths);

  count = sa_size(args->geoms);
  FOR_EACH(i, 0, count) geometry_release(&args->geoms[i]);
  sa_release(args->geoms);
  if(args->ran_geoms) SSP(ranst_discrete_ref_put(args->ran_geoms));
  args->geoms = NULL;
  *args = SCHIFF_ARGS_NULL;
}

