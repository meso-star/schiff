/* Copyright (C) 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015, 2016 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SCHIFF_GEOMETRY_H
#define SCHIFF_GEOMETRY_H

#include <rsys/rsys.h>
#include <star/ssp.h>

enum schiff_param_distribution {
  SCHIFF_PARAM_CONSTANT,
  SCHIFF_PARAM_LOGNORMAL,
  SCHIFF_PARAM_GAUSSIAN,
  SCHIFF_PARAM_HISTOGRAM,
  SCHIFF_PARAM_NONE
};

struct schiff_param {
  enum schiff_param_distribution distribution;
  union {
    double constant;
    struct { double mu, sigma; } lognormal;
    struct { double mu, sigma, range[2]; } gaussian;
    struct { double *entries, lower, upper; } histogram;
  } data;
};
#define SCHIFF_PARAM_DEFAULT__ {SCHIFF_PARAM_CONSTANT, {1.0}}

enum schiff_geometry_type {
  SCHIFF_CYLINDER,
  SCHIFF_ELLIPSOID,
  SCHIFF_HELICAL_PIPE,
  SCHIFF_SPHERE,
  SCHIFF_SUPERSHAPE,

  /* Volume is controlled by a sphere */
  SCHIFF_CYLINDER_AS_SPHERE,
  SCHIFF_ELLIPSOID_AS_SPHERE,
  SCHIFF_HELICAL_PIPE_AS_SPHERE,
  SCHIFF_SUPERSHAPE_AS_SPHERE,

  SCHIFF_NONE
};

/* (x/a)^2 + (y/a)^2 + (z/c)^2 = 1 */
struct schiff_ellipsoid {
  struct schiff_param a;
  struct schiff_param c;

  /* In use by SCHIFF_ELLIPSOID_AS_SPHERE */
  struct schiff_param radius_sphere;

  unsigned nslices;
};

#define SCHIFF_ELLIPSOID_DEFAULT__ \
  {SCHIFF_PARAM_DEFAULT__, SCHIFF_PARAM_DEFAULT__, SCHIFF_PARAM_DEFAULT__, 64}
static const struct schiff_ellipsoid SCHIFF_ELLIPSOID_DEFAULT =
  SCHIFF_ELLIPSOID_DEFAULT__;

struct schiff_sphere {
  struct schiff_param radius;
  unsigned nslices;
};

struct schiff_helical_pipe {
  struct schiff_param pitch; /* Elevation distance of a full revolution */
  struct schiff_param height; /* Total heigh of the helical pipe */
  struct schiff_param radius_helicoid; /* Radius of the helicoid */
  struct schiff_param radius_circle; /* Radius of the meridian circle */

  /* In use by SCHIFF_HELICAL_PIPE_AS_SPHERE */
  struct schiff_param radius_sphere;

  unsigned nslices_helicoid; /* # Discrete steps of the helicoid */
  unsigned nslices_circle; /* # Discrete steps along 2PI */
};

#define SCHIFF_HELICAL_PIPE_DEFAULT__                                          \
  {SCHIFF_PARAM_DEFAULT__,                                                     \
   SCHIFF_PARAM_DEFAULT__,                                                     \
   SCHIFF_PARAM_DEFAULT__,                                                     \
   SCHIFF_PARAM_DEFAULT__,                                                     \
   SCHIFF_PARAM_DEFAULT__,                                                     \
   128, 64}
static const struct schiff_helical_pipe SCHIFF_HELICAL_PIPE_DEFAULT =
  SCHIFF_HELICAL_PIPE_DEFAULT__;

#define SCHIFF_SPHERE_DEFAULT__ {SCHIFF_PARAM_DEFAULT__, 64}
static const struct schiff_sphere SCHIFF_SPHERE_DEFAULT =
  SCHIFF_SPHERE_DEFAULT__;

struct schiff_cylinder {
  struct schiff_param radius;
  struct schiff_param height;

  /* In use by SCHIFF_CYLINDER_AS_SPHERE */
  struct schiff_param radius_sphere;

  unsigned nslices;
};

#define SCHIFF_CYLINDER_DEFAULT__ \
  {SCHIFF_PARAM_DEFAULT__, SCHIFF_PARAM_DEFAULT__, SCHIFF_PARAM_DEFAULT__, 64}
static const struct schiff_cylinder SCHIFF_CYLINDER_DEFAULT =
  SCHIFF_CYLINDER_DEFAULT__;

enum { A, B, M, N0, N1, N2 }; /* Super formula arguments */

struct schiff_supershape {
  struct schiff_param formulas[2][6];
  /* In use by SCHIFF_SUPERSHAPE_AS_SPHERE */
  struct schiff_param radius_sphere;
  unsigned nslices;
};
#define SCHIFF_SUPERSHAPE_DEFAULT__ {                                         \
  {{SCHIFF_PARAM_DEFAULT__,                                                   \
    SCHIFF_PARAM_DEFAULT__,                                                   \
    SCHIFF_PARAM_DEFAULT__,                                                   \
    SCHIFF_PARAM_DEFAULT__,                                                   \
    SCHIFF_PARAM_DEFAULT__,                                                   \
    SCHIFF_PARAM_DEFAULT__},                                                  \
   {SCHIFF_PARAM_DEFAULT__,                                                   \
    SCHIFF_PARAM_DEFAULT__,                                                   \
    SCHIFF_PARAM_DEFAULT__,                                                   \
    SCHIFF_PARAM_DEFAULT__,                                                   \
    SCHIFF_PARAM_DEFAULT__,                                                   \
    SCHIFF_PARAM_DEFAULT__}},                                                 \
   SCHIFF_PARAM_DEFAULT__,                                                    \
   64                                                                         \
}

static const struct schiff_supershape SCHIFF_SUPERSHAPE_DEFAULT =
  SCHIFF_SUPERSHAPE_DEFAULT__;

struct schiff_geometry {
  /* Shape of the geometry */
  enum schiff_geometry_type type;
  union {
    struct schiff_ellipsoid ellipsoid;
    struct schiff_cylinder cylinder;
    struct schiff_helical_pipe helical_pipe;
    struct schiff_sphere sphere;
    struct schiff_supershape supershape;
  } data;
};

#define SCHIFF_GEOMETRY_NULL__ {SCHIFF_NONE, { SCHIFF_ELLIPSOID_DEFAULT__ }}
static const struct schiff_geometry SCHIFF_GEOMETRY_NULL =
SCHIFF_GEOMETRY_NULL__;

/* Forward declarations */
struct s3d_device;
struct schiff_optical_properties;
struct sschiff_geometry_distribution;

extern LOCAL_SYM res_T
schiff_geometry_distribution_init
  (struct sschiff_geometry_distribution* distrib, /* The distribution to init */
   struct s3d_device* s3d,
   const struct schiff_geometry* geometry,
   const size_t ngeoms,
   const double characteristic_length,
   struct ssp_ranst_discrete* ran_geoms,
   struct schiff_optical_properties* properties);

extern LOCAL_SYM void
schiff_geometry_distribution_release
  (struct sschiff_geometry_distribution* distrib);

#endif /* SCHIFF_GEOMETRY_H */

