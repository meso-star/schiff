#!/bin/bash
# Copyright (C) 2015, 2016 CNRS
# Copyright (C) 2019 |Meso|Star>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>. */

tmpfile=schiff_result
tmppipe=schiff_pipe
tmpprefix=xxschiff

nrealisations=1000
nangles=1000
ninvcum=100
ndirs=100
wavelength=0.6 # in micron (in vacuum)
N=1.4666666666666666666666666 # Real refractive index
K=0.0053333333333333333333333 # Imaginary refractive index
Ne=1.3333333333333333333333333 # Refractive index of the medium
sigma=1.18
mean_radius=(1.0 3.22 5.44 7.67 9.89 12.1 14.3 16.6 19.0 21.0)

# Extinction Absorption Scattering
references=(
  "8.39 0.484 7.90"
  "69.3 13.4 55.9"
  "197 54.6 143"
  "392 131 261"
  "651 243 408"
  "975 393 582"
  "1370 578 787"
  "1820 799 1020"
  "2390 1080 1310"
  "2930 1350 1580"
)

# print whether or not the $1 is equal to $2 +/- 4*$3
eq_eps(){
  echo -e\
    "scale=20\n"\
    "dst = $1 - $2\n"\
    "if(dst <0) dst = -dst\n"\
    "dst < 4*$3\n" | bc -l
}

set -e
cd `mktemp -d`
pwd

if [ ! -e $tmppipe ]; then mkfifo $tmppipe; fi

if [ ! -f $tmpfile ]
then
  for mu in ${mean_radius[*]}
  do
    # Generate the geometry distribution into the pipe
    echo -e \
      "sphere:\n" \
      "  radius: { lognormal: { mu: $mu, sigma: $sigma } }" \
      > $tmppipe &

    # invoke the schiff command
    echo $wavelength $N $K $Ne | \
    schiff -q \
      -a $nangles \
      -A $ninvcum \
      -l $mu \
      -i $tmppipe \
      -w $wavelength \
      -g $nrealisations \
      -d $ndirs >> $tmpfile \
      || true # inhibit the "set -e" comportment on failure
  done
fi

err=0 # error code

# Check the estimated cross sections against the references
for ((i=0; i < ${#mean_radius[@]}; ++i))
do
  iline=$(($i*(2*$nangles+$ninvcum+7)+1))
  xsection=(`sed -n "$(($iline))p" $tmpfile | sed 's/^\S*\(.*\)$/\1/g'`)
  reference=(`echo ${references[$i]}`)

  extinction=$(eq_eps ${reference[0]} ${xsection[0]} ${xsection[1]})
  absorption=$(eq_eps ${reference[1]} ${xsection[2]} ${xsection[3]})
  scattering=$(eq_eps ${reference[2]} ${xsection[4]} ${xsection[5]})

  echo -n "${mean_radius[$i]} "
  if [ $extinction -eq 0 -o $absorption -eq 0 -o $scattering -eq 0 ]
  then
    echo FAILURE
    err=1 # notify the error
  else
    echo OK
  fi
done

rm $tmppipe

exit $err
