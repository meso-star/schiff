/* Copyright (C) 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015, 2016 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "schiff_mesh.h"

#include <rsys/float2.h>
#include <rsys/float3.h>
#include <rsys/stretchy_array.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
/* Assume that the vertices are arranged in "phi major" order and that the
 * lower and upper polar vertices are the two last ones. */
static void
setup_sphere_indices
  (struct darray_uint* indices,
   const unsigned nthetas,
   const unsigned nphis)
{
  unsigned i, itheta, iphi;
  ASSERT(indices && nthetas && nphis);

  /* Define the indices of the  contour primitives */
  i = 0;
  FOR_EACH(itheta, 0, nthetas) {
    const unsigned itheta0 = itheta * (nphis - 1);
    const unsigned itheta1 = ((itheta + 1) % nthetas) * (nphis - 1);
    FOR_EACH(iphi, 0, nphis-2) {
      const unsigned iphi0 = iphi + 0;
      const unsigned iphi1 = iphi + 1;

      darray_uint_data_get(indices)[i++] = itheta0 + iphi0;
      darray_uint_data_get(indices)[i++] = itheta0 + iphi1;
      darray_uint_data_get(indices)[i++] = itheta1 + iphi0;

      darray_uint_data_get(indices)[i++] = itheta1 + iphi0;
      darray_uint_data_get(indices)[i++] = itheta0 + iphi1;
      darray_uint_data_get(indices)[i++] = itheta1 + iphi1;
    }
  }

  /* Define the indices of the polar primitives */
  FOR_EACH(itheta, 0, nthetas) {
    const unsigned itheta0 = itheta * (nphis - 1);
    const unsigned itheta1 = ((itheta + 1) % nthetas) * (nphis - 1);

    darray_uint_data_get(indices)[i++] = nthetas * (nphis - 1);
    darray_uint_data_get(indices)[i++] = itheta0;
    darray_uint_data_get(indices)[i++] = itheta1;

    darray_uint_data_get(indices)[i++] = nthetas * (nphis - 1) + 1;
    darray_uint_data_get(indices)[i++] = itheta1 + (nphis - 2);
    darray_uint_data_get(indices)[i++] = itheta0 + (nphis - 2);
  }
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
schiff_mesh_init_sphere
  (struct mem_allocator* allocator,
   struct schiff_mesh* sphere,
   const unsigned nthetas)
{
  /* Theta in [0, 2PI[ and phi in [0, PI[ */
  const unsigned nphis = (unsigned)(((double)nthetas + 0.5) / 2.0);
  const double step_theta = 2*PI / (double)nthetas;
  const double step_phi = PI / (double)nphis;
  size_t nverts;
  size_t ntris;
  size_t i;
  unsigned itheta, iphi;
  res_T res = RES_OK;
  ASSERT(allocator && sphere && nthetas);

  sphere->coordinates = SCHIFF_CARTESIAN;
  darray_float_init(allocator, &sphere->vertices.cartesian);
  darray_uint_init(allocator, &sphere->indices);
  darray_sincos_init(allocator, &sphere->thetas);
  darray_sincos_init(allocator, &sphere->phis);

  nverts = nthetas*(nphis-1)/* #contour verts */ + 2/* polar verts */;
  ntris = 2*nthetas*(nphis-2)/* #contour tris */ + 2*nthetas/* #polar tris */;

  res = darray_float_resize(&sphere->vertices.cartesian, nverts * 3/*#coords*/);
  if(res != RES_OK) goto error;
  res = darray_uint_resize(&sphere->indices, ntris * 3/*#indices per tri*/);
  if(res != RES_OK) goto error;
  res = darray_sincos_resize(&sphere->thetas, nthetas);
  if(res != RES_OK) goto error;
  res = darray_sincos_resize(&sphere->phis, nphis);
  if(res != RES_OK) goto error;

  /* Precompute the cosine/sinus of the theta/phi angles */
  FOR_EACH(itheta, 0, nthetas) {
    const double theta = -PI + (double)itheta * step_theta;
    darray_sincos_data_get(&sphere->thetas)[itheta].angle  = theta;
    darray_sincos_data_get(&sphere->thetas)[itheta].sinus  = sin(theta);
    darray_sincos_data_get(&sphere->thetas)[itheta].cosine = cos(theta);
  }
  FOR_EACH(iphi, 0, nphis-1) {
    const double phi = -PI/2 + (double)(iphi + 1) * step_phi;
    darray_sincos_data_get(&sphere->phis)[iphi].angle  = phi;
    darray_sincos_data_get(&sphere->phis)[iphi].sinus  = sin(phi);
    darray_sincos_data_get(&sphere->phis)[iphi].cosine = cos(phi);
  }

  /* Build the contour vertices */
  i = 0;
  FOR_EACH(itheta, 0, nthetas) {
    const struct sin_cos* theta = darray_sincos_data_get(&sphere->thetas) + itheta;
    FOR_EACH(iphi, 0, nphis-1) {
      const struct sin_cos* phi = darray_sincos_data_get(&sphere->phis) + iphi;
      darray_float_data_get(&sphere->vertices.cartesian)[i++] =
        (float)(theta->cosine * phi->cosine);
      darray_float_data_get(&sphere->vertices.cartesian)[i++] =
        (float)(theta->sinus * phi->cosine);
      darray_float_data_get(&sphere->vertices.cartesian)[i++] =
        (float)phi->sinus;
    }
  }
  /* Setup polar vertices */
  f3(darray_float_data_get(&sphere->vertices.cartesian) + i+0, 0.f, 0.f,-1.f);
  f3(darray_float_data_get(&sphere->vertices.cartesian) + i+3, 0.f, 0.f, 1.f);

  /* Define the indices of the sphere */
  setup_sphere_indices(&sphere->indices, nthetas, nphis);

  sphere->is_init = 1;
exit:
  darray_sincos_release(&sphere->thetas);
  darray_sincos_release(&sphere->phis);
  return res;
error:
  schiff_mesh_release(sphere);
  goto exit;
}

res_T
schiff_mesh_init_sphere_polar
  (struct mem_allocator* allocator,
   struct schiff_mesh* sphere,
   const unsigned nthetas)
{
  /* Theta in [0, 2PI[ and phi in [0, PI[ */
  const unsigned nphis = (unsigned)(((double)nthetas + 0.5) / 2.0);
  const double step_theta = 2*PI / (double)nthetas;
  const double step_phi = PI / (double)nphis;
  size_t nverts;
  size_t ntris;
  size_t i;
  unsigned itheta, iphi;
  res_T res = RES_OK;
  ASSERT(allocator && sphere && nthetas);

  sphere->coordinates = SCHIFF_POLAR;
  darray_uint_init(allocator, &sphere->vertices.polar);
  darray_uint_init(allocator, &sphere->indices);
  darray_sincos_init(allocator, &sphere->thetas);
  darray_sincos_init(allocator, &sphere->phis);

  nverts = nthetas*(nphis-1)/* #contour verts */ + 2/* polar verts */;
  ntris = 2*nthetas*(nphis-2)/* #contour tris */ + 2*nthetas/* #polar tris */;

  res = darray_uint_resize(&sphere->vertices.polar, nverts * 2/* theta phi*/);
  if(res != RES_OK) goto error;
  res = darray_uint_resize(&sphere->indices, ntris * 3/*#indices per tri*/);
  if(res != RES_OK) goto error;
  res = darray_sincos_resize(&sphere->thetas, nthetas);
  if(res != RES_OK) goto error;
  res = darray_sincos_resize(&sphere->phis, nphis + 1/*Polar*/);
  if(res != RES_OK) goto error;

  /* Cosine/Sinus of the theta/phi contour angles */
  FOR_EACH(itheta, 0, nthetas) {
    const double theta = -PI + (double)itheta * step_theta;
    darray_sincos_data_get(&sphere->thetas)[itheta].angle  = theta;
    darray_sincos_data_get(&sphere->thetas)[itheta].sinus  = (float)sin(theta);
    darray_sincos_data_get(&sphere->thetas)[itheta].cosine = (float)cos(theta);
  }
  FOR_EACH(iphi, 0, nphis-1) {
    const double phi = -PI/2 + (double)(iphi + 1) * step_phi;
    darray_sincos_data_get(&sphere->phis)[iphi].angle  = (float)phi;
    darray_sincos_data_get(&sphere->phis)[iphi].sinus  = (float)sin(phi);
    darray_sincos_data_get(&sphere->phis)[iphi].cosine = (float)cos(phi);
  }

  /* Cosine/Sinus of the theta/phi polar angles */
  darray_sincos_data_get(&sphere->phis)[nphis-1].angle  =-(float)PI/2;
  darray_sincos_data_get(&sphere->phis)[nphis-1].sinus  =-1.f;
  darray_sincos_data_get(&sphere->phis)[nphis-1].cosine = 0.f;
  darray_sincos_data_get(&sphere->phis)[nphis-0].angle  = (float)PI/2;
  darray_sincos_data_get(&sphere->phis)[nphis-0].sinus  = 1.f;
  darray_sincos_data_get(&sphere->phis)[nphis-0].cosine = 0.f;

  /* Build the contour vertices */
  i = 0;
  FOR_EACH(itheta, 0, nthetas) {
    FOR_EACH(iphi, 0, nphis-1) {
      darray_uint_data_get(&sphere->vertices.polar)[i++] = itheta;
      darray_uint_data_get(&sphere->vertices.polar)[i++] = iphi;
    }
  }
  /* Setup polar vertices */
  darray_uint_data_get(&sphere->vertices.polar)[i++] = 0;
  darray_uint_data_get(&sphere->vertices.polar)[i++] = nphis-1;
  darray_uint_data_get(&sphere->vertices.polar)[i++] = 0;
  darray_uint_data_get(&sphere->vertices.polar)[i++] = nphis-0;

  /* Define the indices of the sphere */
  setup_sphere_indices(&sphere->indices, nthetas, nphis);

  sphere->is_init = 1;
exit:
  return res;
error:
  schiff_mesh_release(sphere);
  goto exit;
}

res_T
schiff_mesh_init_cylinder
  (struct mem_allocator* allocator,
   struct schiff_mesh* cylinder,
   const unsigned nsteps)
{
  const double step = 2*PI / (double)nsteps;
  size_t nverts;
  size_t ntris;
  unsigned i;
  res_T res = RES_OK;
  ASSERT(allocator && cylinder && nsteps);

  cylinder->coordinates = SCHIFF_CARTESIAN;
  darray_float_init(allocator, &cylinder->vertices.cartesian);
  darray_uint_init(allocator, &cylinder->indices);

  nverts = nsteps*2/* #contour verts */ + 2/* #polar verts */;
  ntris = nsteps*2/* #contour tris */ + 2*nsteps/* #cap tris */;

  res = darray_float_resize(&cylinder->vertices.cartesian, nverts*3/*#coords*/);
  if(res != RES_OK) goto error;
  res = darray_uint_resize(&cylinder->indices, ntris*3/*#indices per tri*/);
  if(res != RES_OK) goto error;

  /* Generate the vertex coordinates */
  FOR_EACH(i, 0, nsteps) {
    const float theta = (float)(i* step);
    const float x = (float)cos(theta);
    const float y = (float)sin(theta);
    f3(darray_float_data_get(&cylinder->vertices.cartesian)+(i*2+0)*3, x,y,0);
    f3(darray_float_data_get(&cylinder->vertices.cartesian)+(i*2+1)*3, x,y,1);
  }

  /* "Polar" vertices */
  f3(darray_float_data_get(&cylinder->vertices.cartesian)+(i*2+0)*3, 0,0,0);
  f3(darray_float_data_get(&cylinder->vertices.cartesian)+(i*2+1)*3, 0,0,1);

  /* Contour primitives */
  FOR_EACH(i, 0, nsteps) {
    const unsigned id = i * 2;
    unsigned* iprim = darray_uint_data_get(&cylinder->indices) + id*3;

    iprim[0] = (id + 0);
    iprim[1] = (id + 1);
    iprim[2] = (id + 2) % (nsteps*2);

    iprim += 3;

    iprim[0] = (id + 2) % (nsteps*2);
    iprim[1] = (id + 1);
    iprim[2] = (id + 3) % (nsteps*2);
  }

  /* Cap primitives */
  FOR_EACH(i, 0, nsteps) {
    const unsigned id = i* 2;
    unsigned* iprim = darray_uint_data_get(&cylinder->indices) + (nsteps + i)*6;

    iprim[0] = (nsteps * 2);
    iprim[1] = (id + 0);
    iprim[2] = (id + 2) % (nsteps*2);

    iprim += 3;

    iprim[0] = (nsteps * 2) + 1;
    iprim[1] = (id + 3) % (nsteps*2);
    iprim[2] = (id + 1);
  }

  cylinder->is_init = 1;
exit:
  return res;
error:
  schiff_mesh_release(cylinder);
  goto exit;
}

res_T
schiff_mesh_init_helical_pipe
  (struct mem_allocator* allocator,
   struct schiff_mesh* helical_pipe,
   const unsigned nsteps_helicoid,
   const unsigned nsteps_circle)

{
  unsigned* indices;
  unsigned* bottom_cap;
  unsigned* top_cap;
  size_t ihelicoid, icircle;
  size_t nverts, ntris;
  size_t ibottom_cap_index, itop_cap_index;
  size_t ibottom_cap_vertex, itop_cap_vertex;
  size_t ilast_meridian_vertex;
  res_T res;
  ASSERT(allocator && helical_pipe && nsteps_helicoid>=2 && nsteps_circle>=4);

  helical_pipe->coordinates = SCHIFF_NO_COORDINATE;
  darray_uint_init(allocator, &helical_pipe->indices);

  nverts = (nsteps_helicoid+1)*nsteps_circle/*#contour*/ + 2/*#cap*/;
  ntris = (nsteps_helicoid)*nsteps_circle*2/*#contour*/ + 2*nsteps_circle/*#cap*/;

  res = darray_uint_resize(&helical_pipe->indices, ntris*3/*#indices per tri*/);
  if(res != RES_OK) goto error;

  indices = darray_uint_data_get(&helical_pipe->indices);

  /* Setup the indices toward the contour primitives */
  FOR_EACH(ihelicoid, 0, nsteps_helicoid) {
    /* Offset toward the first index of the current meridian */
    const size_t offset = ihelicoid * nsteps_circle;

    FOR_EACH(icircle, 0, nsteps_circle) {
      size_t prim_ids[4];
      const size_t id = icircle + offset;
      unsigned* iprim = indices + id*3/*#ids per prim*/*2/*#prims per step*/;

      /* Indices of the quad */
      prim_ids[0] = icircle + offset;
      prim_ids[1] = icircle + nsteps_circle + offset;
      prim_ids[2] = (icircle + 1) % nsteps_circle + nsteps_circle + offset;
      prim_ids[3] = (icircle + 1) % nsteps_circle + offset;
      /* First triangle of the quad */
      iprim[0] = (unsigned)prim_ids[0];
      iprim[1] = (unsigned)prim_ids[3];
      iprim[2] = (unsigned)prim_ids[1];
      /* Second triangule of the quad */
      iprim[3] = (unsigned)prim_ids[1];
      iprim[4] = (unsigned)prim_ids[3];
      iprim[5] = (unsigned)prim_ids[2];
    }
  }

  /* Define the index of the cap vertices */
  ibottom_cap_vertex = nverts - 2; /* Index toward the bottom cap vertex */
  itop_cap_vertex = nverts - 1; /* Index toward the top cap vertex */

  /* Index of the first vertex of the last meridian circle */
  ilast_meridian_vertex = nsteps_helicoid * nsteps_circle;

  ibottom_cap_index = /* Index of the 1st bottom cap index */
    nsteps_helicoid
  * nsteps_circle
  * 2 /*#prims per step*/
  * 3 /*#ids per prim*/;
  itop_cap_index = /* Index of the 1st top cap index */
    ibottom_cap_index
  + nsteps_circle * 3/*# ids per primitive */;

  bottom_cap = indices + ibottom_cap_index ;
  top_cap = indices + itop_cap_index;

  FOR_EACH(icircle, 0, nsteps_circle) {
    const size_t id = icircle * 3/*#ids per prim*/;

    bottom_cap[id + 0] = (unsigned)icircle;
    bottom_cap[id + 1] = (unsigned)ibottom_cap_vertex;
    bottom_cap[id + 2] = (unsigned)((icircle + 1) % nsteps_circle);

    top_cap[id + 0] = (unsigned)((icircle+1)%nsteps_circle + ilast_meridian_vertex);
    top_cap[id + 1] = (unsigned)itop_cap_vertex;
    top_cap[id + 2] = (unsigned)(icircle + ilast_meridian_vertex);
  }

  helical_pipe->is_init = 1;
exit:
  return res;
error:
  schiff_mesh_release(helical_pipe);
  goto exit;
}

res_T
schiff_mesh_helical_pipe_create_vertices
  (const struct schiff_mesh* mesh,
   const double pitch,
   const double height,
   const double hradius, /* Helicoid radius */
   const double cradius, /* Circle radius */
   const unsigned nsteps_helicoid,
   const unsigned nsteps_circle,
   size_t* nvertices,
   float** out_vertices)
{
  double* meridian = NULL;
  float* vertices = NULL;
  double c;
  double phi_max;
  double step_helicoid, step_circle;
  double rcp_sqrt_hradius2_add_c2; /* Precomputed value */
  size_t ihelicoid, icircle;
  size_t icoord, icoord_top, icoord_bottom;
  size_t nverts;
  res_T res = RES_OK;

  ASSERT(mesh && pitch > 0 && height > 0 && hradius > 0 && cradius > 0);
  ASSERT(nvertices && out_vertices);
  ASSERT(mesh->coordinates == SCHIFF_NO_COORDINATE);
  ASSERT(nsteps_helicoid * nsteps_circle * 2 * 3 + nsteps_circle*3*2/*cap*/
      == darray_uint_size_get(&mesh->indices));
  (void)mesh;

  nverts = (nsteps_helicoid+1) * nsteps_circle/*#contour*/ + 2/*#cap*/;
  if(!sa_add(vertices, nverts * 3/*#coords per vertex*/)) {
    res = RES_MEM_ERR;
    goto error;
  }
  if(!sa_add(meridian, nsteps_circle * 3/*#coords per vertex*/)) {
    res = RES_MEM_ERR;
    goto error;
  }

  c = pitch / (2*PI);
  phi_max = height * 2*PI / pitch;
  step_helicoid = phi_max / (double)nsteps_helicoid;
  step_circle = 2*PI / (double)nsteps_circle;
  rcp_sqrt_hradius2_add_c2 = 1.0 / sqrt(hradius*hradius + c*c);

  /* Compute the meridian positions */
  FOR_EACH(icircle, 0, nsteps_circle) {
      const double theta = (double)icircle * step_circle;
      const double cos_theta = cos(theta);
      const double sin_theta = sin(theta);
      const size_t id = icircle *3;

      meridian[id + 0] = cradius * cos_theta + hradius;
      meridian[id + 1] = -cradius * c * rcp_sqrt_hradius2_add_c2 * sin_theta;
      meridian[id + 2] = cradius*hradius * rcp_sqrt_hradius2_add_c2 * sin_theta;
  }

  icoord = 0;
  icoord_bottom =
    (nsteps_helicoid + 1)
   * nsteps_circle
   * 3/*#coords per vertex*/;
  icoord_top = icoord_bottom + 3/*#coords per vertex */;

  FOR_EACH(ihelicoid, 0, nsteps_helicoid + 1) {
    const double phi = (double)ihelicoid * step_helicoid;
    const double cos_phi = cos(phi);
    const double sin_phi = sin(phi);

    FOR_EACH(icircle, 0, nsteps_circle) {
      const double* pos = meridian + icircle*3;
      vertices[icoord + 0] = (float)(pos[0]*cos_phi - pos[1]*sin_phi);
      vertices[icoord + 1] = (float)(pos[0]*sin_phi + pos[1]*cos_phi);
      vertices[icoord + 2] = (float)(pos[2] + c * phi);
      icoord += 3;
    }

    /* Top cap vertex */
    if(ihelicoid == 0) {
      vertices[icoord_bottom + 0] = (float)(hradius * cos_phi);
      vertices[icoord_bottom + 1] = (float)(hradius * sin_phi);
      vertices[icoord_bottom + 2] = (float)(c*phi);

    /* Bottom cap vertex */
    } else if(ihelicoid == nsteps_helicoid) {
      vertices[icoord_top + 0] = (float)(hradius * cos_phi);
      vertices[icoord_top + 1] = (float)(hradius * sin_phi);
      vertices[icoord_top + 2] = (float)(c * phi);
    }
  }

exit:
  if(meridian) sa_release(meridian);
  *nvertices = nverts;
  *out_vertices = vertices;
  return res;
error:
  if(vertices) {
    sa_release(vertices);
    vertices = NULL;
    nverts = 0;
  }
  goto exit;
}

void
schiff_mesh_helical_pipe_destroy_vertices
  (const struct schiff_mesh* mesh, float* out_vertices)
{
  ASSERT(mesh && out_vertices);
  (void)mesh;
  sa_release(out_vertices);
}

void
schiff_mesh_release(struct schiff_mesh* mesh)
{
  ASSERT(mesh);
  if(!mesh->is_init)
    return; /* The mesh is not initialised */

  switch(mesh->coordinates) {
    case SCHIFF_CARTESIAN:
      darray_float_release(&mesh->vertices.cartesian);
      break;
    case SCHIFF_POLAR:
      darray_uint_release(&mesh->vertices.polar);
      darray_sincos_release(&mesh->thetas);
      darray_sincos_release(&mesh->phis);
      break;
    case SCHIFF_NO_COORDINATE: /* Do nothing */ break;
    default: FATAL("Unreachable code\n"); break;
  }
  darray_uint_release(&mesh->indices);
  mesh->coordinates = SCHIFF_NO_COORDINATE;
  mesh->is_init = 0;
}

