/* Copyright (C) 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015, 2016 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SCHIFF_MESH_H
#define SCHIFF_MESH_H

#include <rsys/dynamic_array_float.h>
#include <rsys/dynamic_array_uint.h>

struct sin_cos { double angle, sinus, cosine; };

#define DARRAY_NAME sincos
#define DARRAY_DATA struct sin_cos
#include <rsys/dynamic_array.h>

enum schiff_coordinates {
  SCHIFF_NO_COORDINATE, /* No coordinate is setuped */
  SCHIFF_POLAR,
  SCHIFF_CARTESIAN
};

struct schiff_mesh {
  enum schiff_coordinates coordinates;
  union {
    struct darray_float cartesian;
    struct darray_uint polar;
  } vertices;
  struct darray_uint indices;

  /* Used only by polar coordinates */
  struct darray_sincos thetas; /* List of thetas, cos(theta) , sin(theta) */
  struct darray_sincos phis; /* List of phis, cos(phi), sin(phi) */

  int is_init;
};

extern LOCAL_SYM res_T
schiff_mesh_init_sphere
  (struct mem_allocator* allocator,
   struct schiff_mesh* mesh,
   const unsigned nthetas); /* # discret points along 2PI */

extern LOCAL_SYM res_T
schiff_mesh_init_sphere_polar
  (struct mem_allocator* allocator,
   struct schiff_mesh* mesh,
   const unsigned nthetas);

extern LOCAL_SYM res_T
schiff_mesh_init_cylinder
  (struct mem_allocator* allocator,
   struct schiff_mesh* mesh,
   const unsigned nslices);

/* Initialise the indices of the helical pipe. The vertices are still not
 * defined. Use the schiff_mesh_setup_helical_pipe to define these data */
extern LOCAL_SYM res_T
schiff_mesh_init_helical_pipe
  (struct mem_allocator* allocator,
   struct schiff_mesh* mesh,
   const unsigned nsteps_helicoid,
   const unsigned nsteps_circle);

extern LOCAL_SYM res_T
schiff_mesh_helical_pipe_create_vertices
  (const struct schiff_mesh* mesh,
   const double pitch,
   const double height,
   const double radius_helicoid,
   const double radius_circle,
   /* The 2 following attribs are assumed to be equal to attributes used by the
    * init function */
   const unsigned nsteps_helicoid,
   const unsigned nsteps_circle,
   size_t* out_nvertices,
   float** out_vertices);

extern LOCAL_SYM void
schiff_mesh_helical_pipe_destroy_vertices
  (const struct schiff_mesh* mesh,
   float* vertices);

extern LOCAL_SYM void
schiff_mesh_release
  (struct schiff_mesh* mesh);

static INLINE void
schiff_mesh_get_indices
  (const struct schiff_mesh* mesh,
   const unsigned itri,
   unsigned ids[3])
{
  const size_t i = itri * 3;
  ASSERT(mesh);
  ASSERT(darray_uint_size_get(&mesh->indices) % 3 == 0);
  ASSERT(itri < darray_uint_size_get(&mesh->indices) / 3);
  ids[0] = darray_uint_cdata_get(&mesh->indices)[i + 0];
  ids[1] = darray_uint_cdata_get(&mesh->indices)[i + 1];
  ids[2] = darray_uint_cdata_get(&mesh->indices)[i + 2];
}

static INLINE void
schiff_mesh_get_cartesian_position
  (const struct schiff_mesh* mesh,
   const unsigned ivert,
   float vertex[3])
{
  const size_t i = ivert * 3;
  ASSERT(mesh && vertex && mesh->coordinates == SCHIFF_CARTESIAN);
  ASSERT(darray_float_size_get(&mesh->vertices.cartesian) % 3 == 0);
  ASSERT(ivert < darray_float_size_get(&mesh->vertices.cartesian) / 3);
  vertex[0] = darray_float_cdata_get(&mesh->vertices.cartesian)[i + 0];
  vertex[1] = darray_float_cdata_get(&mesh->vertices.cartesian)[i + 1];
  vertex[2] = darray_float_cdata_get(&mesh->vertices.cartesian)[i + 2];
}

static INLINE void
schiff_mesh_get_polar_position
  (const struct schiff_mesh* mesh,
   const unsigned ivert,
   struct sin_cos angles[2])
{
  const size_t i = ivert * 2;
  const unsigned* iangles;
  ASSERT(mesh && angles && mesh->coordinates == SCHIFF_POLAR);
  ASSERT(darray_uint_size_get(&mesh->vertices.polar) % 2 == 0);
  ASSERT(ivert < darray_uint_size_get(&mesh->vertices.polar) / 2);

  iangles = darray_uint_cdata_get(&mesh->vertices.polar) + i;
  angles[0] = darray_sincos_cdata_get(&mesh->thetas)[iangles[0]];
  angles[1] = darray_sincos_cdata_get(&mesh->phis)[iangles[1]];
}

#endif /* SBOX_SCHIFF_MESH_H */

