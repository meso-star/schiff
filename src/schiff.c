/* Copyright (C) 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015, 2016 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "schiff_args.h"
#include "schiff_geometry.h"
#include "schiff_optical_properties.h"

#include <rsys/clock_time.h>
#include <rsys/stretchy_array.h>

#include <star/s3d.h>
#include <star/sschiff.h>
#include <star/ssp.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static res_T
dump_geometries
  (const struct schiff_args* args,
   struct s3d_device* s3d,
   struct sschiff_geometry_distribution* distrib,
   struct ssp_rng* rng,
   FILE* stream)
{
  struct s3d_scene* scn = NULL;
  struct s3d_shape* shape = NULL;
  unsigned i;
  res_T res = RES_OK;
  ASSERT(args && distrib && rng && stream);

  res = s3d_scene_create(s3d, &scn);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't create the Star-3D scene.\n");
    goto error;
  }
  res = s3d_shape_create_mesh(s3d, &shape);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't create the Star-3D shape.\n");
    goto error;
  }
  res = s3d_scene_attach_shape(scn, shape);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't attach the Star-3D shape to the Star-3D scene.\n");
    goto error;
  }

  FOR_EACH(i, 0, args->ngeoms_dump) {
    unsigned ivert, nverts;
    unsigned itri, ntris;
    double volume_scaling;
    double dist_scaling;
    void* shape_data = NULL;
    res = distrib->sample(rng, &shape_data, shape, distrib->context);
    if(res != RES_OK) {
      fprintf(stderr, "Couldn't sample the micro organism geometry.\n");
      goto error;
    }
    res = distrib->sample_volume_scaling
      (rng, shape_data, &volume_scaling, distrib->context);
    if(res != RES_OK) {
      fprintf(stderr, "Couldn't sample the volume scaling of the geometry.\n");
      goto error;
    }

    dist_scaling = pow(volume_scaling, 1.0/3.0);

    fprintf(stream, "g schiff_geometry_%u\n", i);

    /* Dump vertex position */
    fprintf(stream, "# List of vertex positions\n");
    S3D(mesh_get_vertices_count(shape, &nverts));
    FOR_EACH(ivert, 0, nverts) {
      struct s3d_attrib attr;
      S3D(mesh_get_vertex_attrib(shape, ivert, S3D_POSITION, &attr));
      f3_mulf(attr.value, attr.value, (float)dist_scaling);
      fprintf(stream, "v %f %f %f\n", SPLIT3(attr.value));
    }

    /* Dump triangle indices */
    fprintf(stream, "# Vertex indices of the triangular faces\n");
    S3D(mesh_get_triangles_count(shape, &ntris));
    FOR_EACH(itri, 0, ntris) {
      unsigned ids[3];
      S3D(mesh_get_triangle_indices(shape, itri, ids));
      fprintf(stream, "f %u %u %u\n", ids[0]+1, ids[1]+1, ids[2]+1);
    }
  }

exit:
  if(scn) S3D(scene_ref_put(scn));
  if(shape) S3D(shape_ref_put(shape));
  return res;
error:
  goto exit;
}

static void
write_cross_sections
  (FILE* stream,
   struct sschiff_estimator* estimator,
   const double* wlens, /* List of wavelengths in microns */
   const size_t nwlens) /* #wavelengths */
{
  size_t iwlen;
  ASSERT(stream && estimator && wlens && nwlens);

  FOR_EACH(iwlen, 0, nwlens) {
    const struct sschiff_state* val;
    struct sschiff_cross_section cross_section;

    SSCHIFF(estimator_get_cross_section(estimator, iwlen, &cross_section));
    fprintf(stream, "%g ", wlens[iwlen]);

    val = &cross_section.extinction;
    fprintf(stream, "%g %g ", val->E, val->SE);
    val = &cross_section.absorption;
    fprintf(stream, "%g %g ", val->E, val->SE);
    val = &cross_section.scattering;
    fprintf(stream, "%g %g ", val->E, val->SE);
    val = &cross_section.average_projected_area;
    fprintf(stream, "%g %g\n", val->E, val->SE);
  }
  fprintf(stream, "\n");
}

static void
write_phase_function_descriptors
  (FILE* stream,
   struct sschiff_estimator* estimator,
   const double* wlens, /* List of wavelengths in microns */
   const size_t nwlens, /* #wavelenghts */
   const size_t nangles_inv) /* Number of inversed cumulative entries */
{
  size_t iwlen;
  const double* angles;
  size_t nangles;
  res_T res = RES_OK;
  ASSERT(stream && estimator && wlens && nwlens && nangles_inv > 1);

  SSCHIFF(estimator_get_scattering_angles(estimator, &angles, &nangles));

  FOR_EACH(iwlen, 0, nwlens) {
    size_t iangle;
    struct sschiff_state cdf, pf;
    double n;

    res = sschiff_estimator_get_limit_scattering_angle_index
      (estimator, iwlen, &iangle);

    if(res == RES_BAD_OP) {
      /* No valid limit angle => no phase function. Print -1 for all the data */
      fprintf(stderr,
        "The phase function couldn't be computed.\n"
        "wavelength = %g microns\n", wlens[iwlen]);
      fprintf(stream, "-1 -1 -1 -1 -1 -1 -1 -1 -1\n");

    } else {
      /* Retrieve the phase function descriptor data */
      ASSERT(res == RES_OK);
      SSCHIFF(estimator_get_wide_scattering_angle_model_parameter
        (estimator, iwlen, &n));
      SSCHIFF(estimator_get_differential_cross_section
        (estimator, iwlen, iangle, &pf));
      SSCHIFF(estimator_get_differential_cross_section_cumulative
        (estimator, iwlen, iangle, &cdf));

      /* Write the phase function descriptor */
      fprintf(stream, "%g %g %g %g %g %g %g %lu %lu\n",
        wlens[iwlen], angles[iangle], pf.E, pf.SE, cdf.E, cdf.SE, n,
        (unsigned long)nangles,
        (unsigned long)nangles_inv);
    }
  }
  fprintf(stream, "\n"); /* Empty line */
}

static void
write_phase_functions
  (FILE* stream,
   const struct sschiff_estimator* estimator,
   const double* wlens,
   const size_t nwlens)
{
  size_t iwlen;
  const double* angles;
  const struct sschiff_state* phase_funcs;
  size_t iangle, nangles;
  ASSERT(stream && estimator && wlens && nwlens);
  (void)wlens;

  SSCHIFF(estimator_get_scattering_angles(estimator, &angles, &nangles));
  FOR_EACH(iwlen, 0, nwlens) {
    const res_T res = sschiff_estimator_get_phase_function
      (estimator, iwlen, &phase_funcs);
    if(res == RES_BAD_OP) {
      /* The phase function couldn't be computed. Write default data. */
      FOR_EACH(iangle, 0, nangles) {
        fprintf(stream, "-1 -1 -1\n");
      }
    } else {
      ASSERT(res == RES_OK);
      FOR_EACH(iangle, 0, nangles) {
        fprintf(stream, "%g %g %g\n",
          angles[iangle],
          phase_funcs[iangle].E,
          phase_funcs[iangle].SE);
      }
    }
    fprintf(stream, "\n");
  }
}

static void
write_phase_functions_cum
  (FILE* stream,
   struct sschiff_estimator* estimator,
   const double* wlens,
   const size_t nwlens)
{
  size_t iwlen;
  const double* angles;
  const struct sschiff_state* phase_funcs_cum;
  size_t iangle, nangles;
  ASSERT(stream && estimator && wlens && nwlens);
  (void)wlens;

  SSCHIFF(estimator_get_scattering_angles(estimator, &angles, &nangles));
  FOR_EACH(iwlen, 0, nwlens) {
    const res_T res = sschiff_estimator_get_phase_function_cumulative
      (estimator, iwlen, &phase_funcs_cum);
    if(res == RES_BAD_OP) {
      /* The cumulative phase func couldn't be computed. Write default data */
      FOR_EACH(iangle, 0, nangles) {
        fprintf(stream, "-1 -1 -1\n");
      }
    } else {
      ASSERT(res == RES_OK);
      FOR_EACH(iangle, 0, nangles) {
        fprintf(stream, "%g %g %g\n",
          angles[iangle],
          phase_funcs_cum[iangle].E,
          phase_funcs_cum[iangle].SE);
      }
    }
    fprintf(stream, "\n");
  }
}

static void
write_phase_functions_invcum
  (FILE* stream,
   struct sschiff_estimator* estimator,
   const double* wlens,
   const size_t nwlens,
   const size_t nangles_inv)
{
  double* invcum = NULL;
  size_t iangle;
  size_t iwlen;
  double step;
  ASSERT(stream && estimator && wlens && nwlens && nangles_inv > 1);

  step = 1.0 / (double)(nangles_inv - 1);

  if(!sa_add(invcum, nangles_inv)) {
    fprintf(stderr,
      "Couldn't allocate the list of inverse cumulative phase function angles.\n");

    /* Print the default -1 value */
    FOR_EACH(iwlen, 0, nwlens) {
      FOR_EACH(iangle, 0, nangles_inv) {
        fprintf(stream, "-1 -1\n");
      }
      fprintf(stream, "\n");
    }
  } else {
    FOR_EACH(iwlen, 0, nwlens) {
      const res_T res = sschiff_estimator_inverse_cumulative_phase_function
        (estimator, iwlen, invcum, nangles_inv);
      if(res != RES_OK) {
        /* The inverse cumulative cannot be computed. Write -1 */
        fprintf(stderr,
          "Couldn't inverse the cumulative phase function.\n"
          "wavelength = %g microns\n", wlens[iwlen]);
        FOR_EACH(iangle, 0, nangles_inv) {
          fprintf(stream, "-1 -1\n");
        }
      } else {
        /* Write the inverse cumulative values */
        FOR_EACH(iangle, 0, nangles_inv-1) {
          fprintf(stream, "%g %g\n", (double)iangle*step, invcum[iangle]);
        }
        /* Handle precision issues */
        fprintf(stream, "1 %g\n", invcum[nangles_inv-1]);
      }
      fprintf(stream, "\n"); /* Empty line */
    }
  }

  if(invcum)
    sa_release(invcum);
}

static res_T
run_integration
  (const struct schiff_args* args,
   struct s3d_device* s3d,
   struct sschiff_geometry_distribution* distrib,
   struct ssp_rng* rng,
   FILE* stream)
{
  char dump[128];
  struct time t0, t1;
  struct sschiff_device* sschiff = NULL;
  struct sschiff_estimator* estimator = NULL;
  size_t nwlens;
  res_T res = RES_OK;
  ASSERT(args && sa_size(args->wavelengths) && rng && stream);

  res = sschiff_device_create(NULL, NULL, args->nthreads, 1, s3d, &sschiff);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't create the Star Schiff device.\n");
    goto error;
  }

  time_current(&t0);

  /* Invoke the Schiff integration */
  nwlens = sa_size(args->wavelengths);
  res = sschiff_integrate(sschiff, rng, distrib, args->wavelengths, nwlens,
    sschiff_uniform_scattering_angles, args->nangles, args->nrealisations,
    args->ninsamps, args->discard_large_angles, &estimator);
  if(res != RES_OK) {
    fprintf(stderr, "Schiff integration error.\n");
    goto error;
  }

  time_sub(&t0, time_current(&t1), &t0);
  time_dump(&t0, TIME_ALL, NULL, dump, sizeof(dump));
  fprintf(stderr, "Computation performed in %s.\n", dump);

  /* Write results */
  write_cross_sections(stream, estimator, args->wavelengths, nwlens);
  write_phase_function_descriptors
    (stream, estimator, args->wavelengths, nwlens, args->nangles_inv);
  write_phase_functions(stream, estimator, args->wavelengths, nwlens);
  write_phase_functions_cum(stream, estimator, args->wavelengths, nwlens);
  write_phase_functions_invcum
    (stream, estimator, args->wavelengths, nwlens, args->nangles_inv);

exit:
  if(estimator) SSCHIFF(estimator_ref_put(estimator));
  if(sschiff) SSCHIFF(device_ref_put(sschiff));
  return res;
error:
  goto exit;
}

static res_T
run(const struct schiff_args* args)
{
  char dump[128];
  struct time t0, t1;
  FILE* fp = stdout;
  struct sschiff_geometry_distribution distrib = SSCHIFF_NULL_GEOMETRY_DISTRIBUTION;
  struct ssp_rng* rng = NULL;
  struct s3d_device* s3d = NULL;
  res_T res = RES_OK;
  ASSERT(args);

  if(args->output_filename) {
    fp = fopen(args->output_filename, "w");
    if(!fp) {
      fprintf(stderr,
        "Couldn't open the output file `%s'.\n", args->output_filename);
      res = RES_IO_ERR;
      goto error;
    }
  }

  res = ssp_rng_create(&mem_default_allocator, SSP_RNG_THREEFRY, &rng);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't create the Random Number Generator.\n");
    goto error;
  }

  res = s3d_device_create(NULL, &mem_default_allocator, 0, &s3d);
  if(res != RES_OK) {
    fprintf(stderr, "Couldn't create the Star-3D device.\n");
    goto error;
  }

  time_current(&t0);

  res = schiff_geometry_distribution_init(&distrib, s3d, args->geoms,
    sa_size(args->geoms), args->characteristic_length, args->ran_geoms,
    args->properties);
  if(res != RES_OK) {
    fprintf(stderr,
      "Couldn't initialise the Star Schiff geometry distribution.\n");
    goto error;
  }

  time_sub(&t0, time_current(&t1), &t0);
  time_dump(&t0, TIME_ALL, NULL, dump, sizeof(dump));
  fprintf(stderr, "Initialized the geometry distribution in %s.\n", dump);

  if(!args->ngeoms_dump) {
    res = run_integration(args, s3d, &distrib, rng, fp);
  } else {
    time_current(&t0);
    res = dump_geometries(args, s3d, &distrib, rng, fp);
    time_sub(&t0, time_current(&t1), &t0);
    time_dump(&t0, TIME_ALL, NULL, dump, sizeof(dump));
    fprintf(stderr, "Dump geometries in %s.\n", dump);
  }

  /* Release before the check of the integration error code since if an error
   * occurred the function will return without releasing the distribution. */
  schiff_geometry_distribution_release(&distrib);

  if(res != RES_OK)
    goto error;

exit:
  if(fp && fp != stdout) fclose(fp);
  if(rng) SSP(rng_ref_put(rng));
  if(s3d) S3D(device_ref_put(s3d));
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Entry point
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct schiff_args args = SCHIFF_ARGS_NULL;
  int err = 0;
  res_T res;

  res = schiff_args_init(&args, argc, argv);
  if(res != RES_OK) goto error;
  if(!args.ngeoms_dump && !args.wavelengths) goto exit;
  res = run(&args);
  if(res != RES_OK) goto error;

exit:
  schiff_args_release(&args);
  if(mem_allocated_size() != 0) {
    fprintf(stderr, "Memory leaks: %lu Bytes\n",
      (unsigned long) mem_allocated_size());
  }
  return err;
error:
  err = 1;
  goto exit;
}

