/* Copyright (C) 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015, 2016 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SCHIFF_STREAMLINE_H
#define SHCIFF_STREAMLINE_H

#include <rsys/stretchy_array.h>
#include <string.h>

struct schiff_streamline { char* buf; };

static INLINE void
schiff_streamline_init(struct schiff_streamline* sline)
{
  ASSERT(sline);
  memset(sline, 0, sizeof(struct schiff_streamline));
  sline->buf = sa_add(sline->buf, 128);
}

static INLINE void
schiff_streamline_release(struct schiff_streamline* sline)
{
  ASSERT(sline);
  sa_release(sline->buf);
}

static INLINE res_T
schiff_streamline_read
  (struct schiff_streamline* sline,
   FILE* stream,
   char** out_line)
{
  char* line = NULL;
  size_t last_char;
  const size_t chunk = 128;
  res_T res = RES_OK;
  ASSERT(sline && stream && out_line);

  if(!fgets(sline->buf, (int)sa_size(sline->buf), stream)) {
    res = RES_EOF;
    goto exit;
  }

  while(!strrchr(sline->buf, '\n')) { /* Ensure that the whole line is read */
    if(!fgets(sa_add(sline->buf, chunk), (int)chunk, stream)) /* EOF */
      break;
  }

  /* Remove leading spaces */
  line = sline->buf;
  while((*line == ' ' || *line == '\t') && *line != '\0') ++line;

  /* Remove newline character(s) */
  last_char = strlen(line);
  while(last_char-- && (line[last_char]=='\n' || line[last_char]=='\r'));
  line[last_char + 1] = '\0';

exit:
  *out_line = line;
  return res;
error:
  goto error;
}

#endif /* SCHIFF_STREAMLINE_H */

