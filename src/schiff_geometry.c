/* Copyright (C) 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015, 2016 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "schiff_geometry.h"
#include "schiff_mesh.h"
#include "schiff_optical_properties.h"

#include <rsys/algorithm.h>
#include <rsys/stretchy_array.h>

#include <star/s3d.h>
#include <star/ssp.h>
#include <star/sschiff.h>

/* 3D Context of an ellipsoid */
struct ellipsoid_context {
  /* Precomputed values used to define the radius of the ellipsoid for a
   * {theta, phi} position :
   *  r = (a*b*c)
   *    / sqrt(b^2*c^2*cos(theta)^2*cos(phi)^2
   *         + a^2*c^2*sin(theta)^2*cos(phi)^2
   *         + a^2*b^2*sin(phi)^2) */
  double abc;  /* a*b*c */
  double b2c2; /* b^2*c^2 */
  double a2c2; /* a^2*c^2 */
  double a2b2; /* a^2*b^2 */
};

/* 3D Context of the sphere geometry */
struct sphere_context {
  float radius; /* Sphere radius */
};

/* 3D Context of the cylinder geometry */
struct cylinder_context {
  float radius;
  float height;
};

/* 3D Context of the supershape geometry */
struct supershape_context {
  double formulas[2][6];
};

/* Distribution context of a cylinder geometry */
struct cylinder_distribution_context {
  double log_mean_radius; /* Log of the mean radius */
  double log_sigma; /* Log of the sigma argument of the lognormal distribution*/
  double aspect_ratio; /* aspect ratio of the cylinder distribution */
};

/* 3D context of a generic geometry */
struct mesh_context {
  const struct schiff_mesh* mesh; /* Triangular mesh of the geometry */
  const struct schiff_geometry* geometry;
  enum schiff_geometry_type type;
  union {
    struct ellipsoid_context ellipsoid;
    struct cylinder_context cylinder;
    struct sphere_context sphere;
    struct supershape_context supershape;
  } data;
};

/* Specific mesh context for the helical pipe */
struct helical_pipe_mesh_context {
  const unsigned* indices;
  const float* vertices;
};


struct shape {
  const struct schiff_geometry* geometry; /* Pointer onto the geometry */
  struct s3d_shape* shape; /* May be NULL => No volume distribution */
  struct schiff_mesh mesh; /* Triangular mesh of the shape */
  double volume; /* Volume of the shape */
};

/* Distribution context of a generic geometry */
struct geometry_distribution_context {
  struct schiff_optical_properties* properties; /* Per wavelength properties */
  struct shape* shapes; /* List of shapes */
  struct ssp_ranst_discrete* ran_geometries; /* Geometries random variates */
};

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static int
cmp_double(const void* op0, const void* op1)
{
  const double* a = (const double*)op0;
  const double* b = (const double*)op1;
  return *a < *b ? -1 : (*a > *b ? 1 : 0);
}

static double
histogram_sample
  (const double* entries,
   const size_t nentries,
   const double lower,
   const double upper,
   const double u)
{
  const double* find;
  double step;
  double from, to;
  double v;
  ASSERT(entries && nentries && lower < upper && u >= 0 && u < 1.0);

  /* Search for the first entry that is gequal than the canonical variable u */
  find = search_lower_bound(&u, entries, nentries, sizeof(double), cmp_double);

  if(!find) /* Handle numerical issue */
    find = entries + (nentries - 1);

  step = (upper - lower) / (double)nentries; /* Size of an histogram interval */
  from = lower + (double)(find - entries) * step;
  to = from + step;

  /* Linear interpolation */
  v = CLAMP((u - from) / step, 0.0, 1.0);
  if(eq_eps(v, 0, 1.e-6)) {
    return from;
  } else if(eq_eps(v, 1, 1.e-6)) {
    return to;
  } else {
    return v*to + (1 - v)*from;
  }
}

static FINLINE double
ran_gaussian_truncated
  (struct ssp_rng* rng,
   const double mu,
   const double sigma,
   const double range[2])
{
  double val;
  ASSERT(rng && mu > 0 && mu >= range[0] && mu <= range[1]);
  do {
    val = ssp_ran_gaussian(rng, mu, sigma);
  } while(val < range[0] || val > range[1] );
  return val;
}

static INLINE double
eval_param(const struct schiff_param* param, struct ssp_rng* rng)
{
  double val = 0.0;
  ASSERT(param);

  switch(param->distribution) {
    case SCHIFF_PARAM_CONSTANT:
      val = param->data.constant;
      break;
    case SCHIFF_PARAM_LOGNORMAL:
      val = ssp_ran_lognormal(rng, log(param->data.lognormal.mu),
        log(param->data.lognormal.sigma));
      break;
    case SCHIFF_PARAM_GAUSSIAN:
      val = ran_gaussian_truncated(rng, param->data.gaussian.mu,
        param->data.gaussian.sigma, param->data.gaussian.range);
      break;
    case SCHIFF_PARAM_HISTOGRAM:
      val = histogram_sample
        (param->data.histogram.entries,
         sa_size(param->data.histogram.entries),
         param->data.histogram.lower,
         param->data.histogram.upper,
         ssp_rng_canonical(rng));
      break;
    default: FATAL("Unreachable code.\n"); break;
  }
  return val;
}

static void
get_material_property
  (void* mtl,
   const double wavelength,
   struct sschiff_material_properties* props)
{
  struct schiff_optical_properties* properties = mtl;
  schiff_optical_properties_fetch(properties, wavelength, props);
}

static INLINE void
geometry_get_indices(const unsigned itri, unsigned ids[3], void* ctx)
{
  struct mesh_context* mesh_ctx = ctx;
  ASSERT(ctx);
  schiff_mesh_get_indices(mesh_ctx->mesh, itri, ids);
}

static INLINE void
geometry_get_position(const unsigned ivert, float vertex[3], void* ctx)
{
  struct mesh_context* mesh_ctx = ctx;
  ASSERT(ctx);
  schiff_mesh_get_cartesian_position(mesh_ctx->mesh, ivert, vertex);
}

static void
ellipsoid_get_position(const unsigned ivert, float vertex[3], void* ctx)
{
  struct mesh_context* mesh_ctx = ctx;
  struct sin_cos angles[2];
  double sqr_cos_theta;
  double sqr_cos_phi;
  double sqr_sin_theta;
  double sqr_sin_phi;
  double radius;
  double tmp;
  ASSERT(mesh_ctx && mesh_ctx->type == SCHIFF_ELLIPSOID);

  schiff_mesh_get_polar_position(mesh_ctx->mesh, ivert, angles);
  sqr_cos_theta = angles[0].cosine * angles[0].cosine;
  sqr_sin_theta = angles[0].sinus * angles[0].sinus;
  sqr_cos_phi = angles[1].cosine * angles[1].cosine;
  sqr_sin_phi = angles[1].sinus * angles[1].sinus;

  tmp = mesh_ctx->data.ellipsoid.b2c2 * sqr_cos_theta * sqr_cos_phi
      + mesh_ctx->data.ellipsoid.a2c2 * sqr_sin_theta * sqr_cos_phi
      + mesh_ctx->data.ellipsoid.a2b2 * sqr_sin_phi;
  radius = mesh_ctx->data.ellipsoid.abc / sqrt(tmp);

  vertex[0] = (float)(angles[0].cosine * angles[1].cosine * radius);
  vertex[1] = (float)(angles[0].sinus * angles[1].cosine * radius);
  vertex[2] = (float)(angles[1].sinus * radius);
}

static void
cylinder_get_position(const unsigned ivert, float vertex[3], void* ctx)
{
  struct mesh_context* mesh_ctx = ctx;
  ASSERT(mesh_ctx && mesh_ctx->type == SCHIFF_CYLINDER);
  schiff_mesh_get_cartesian_position(mesh_ctx->mesh, ivert, vertex);
  vertex[0] *= mesh_ctx->data.cylinder.radius;
  vertex[1] *= mesh_ctx->data.cylinder.radius;
  vertex[2] *= mesh_ctx->data.cylinder.height;
}

static void
helical_pipe_get_indices(const unsigned itri, unsigned ids[3], void* ctx)
{
  size_t i = itri * 3/*#indices per triangle*/;
  const struct helical_pipe_mesh_context* mesh_ctx = ctx;
  ASSERT(mesh_ctx);
  ids[0] = mesh_ctx->indices[i + 0];
  ids[1] = mesh_ctx->indices[i + 1];
  ids[2] = mesh_ctx->indices[i + 2];
}

static void
helical_pipe_get_position(const unsigned ivert, float vertex[3], void* ctx)
{
  const size_t i = ivert * 3/*#coords*/;
  const struct helical_pipe_mesh_context* mesh_ctx = ctx;
  ASSERT(mesh_ctx);
  vertex[0] = mesh_ctx->vertices[i + 0];
  vertex[1] = mesh_ctx->vertices[i + 1];
  vertex[2] = mesh_ctx->vertices[i + 2];
}

static void
sphere_get_position(const unsigned ivert, float vertex[3], void* ctx)
{
  struct mesh_context* mesh_ctx = ctx;
  ASSERT(mesh_ctx && mesh_ctx->type == SCHIFF_SPHERE);
  schiff_mesh_get_cartesian_position(mesh_ctx->mesh, ivert, vertex);
  vertex[0] *= mesh_ctx->data.sphere.radius;
  vertex[1] *= mesh_ctx->data.sphere.radius;
  vertex[2] *= mesh_ctx->data.sphere.radius;
}

static void
supershape_get_position(const unsigned ivert, float vertex[3], void* ctx)
{
  struct mesh_context* mesh_ctx = ctx;
  struct sin_cos angles[2];
  double uv[2];
  int iform;
  ASSERT(mesh_ctx && mesh_ctx->type == SCHIFF_SUPERSHAPE);

  schiff_mesh_get_polar_position(mesh_ctx->mesh, ivert, angles);

  FOR_EACH(iform, 0, 2) {
    double m, k, g;
    double* form = mesh_ctx->data.supershape.formulas[iform];

    m = cos(form[M] * angles[iform].angle / 4.0);
    m = fabs(m) / form[A];
    k = sin(form[M] * angles[iform].angle / 4.0);
    k = fabs(k) / form[B];
    g = pow(m, form[N1]) + pow(k, form[N2]);
    uv[iform] = pow(g, (-1.0/form[N0]));
  }

  vertex[0] = (float)(uv[0] * angles[0].cosine * uv[1] * angles[1].cosine);
  vertex[1] = (float)(uv[0] * angles[0].sinus  * uv[1] * angles[1].cosine);
  vertex[2] = (float)(uv[1] * angles[1].sinus);
}

static res_T
compute_s3d_shape_volume
  (struct s3d_device* s3d, struct s3d_shape* shape, double* out_volume)
{
  struct s3d_scene* scn = NULL;
  struct s3d_scene_view* view = NULL;
  float volume = 0.f;
  res_T res = RES_OK;
  ASSERT(s3d && shape && out_volume);

  if(RES_OK != (res = s3d_scene_create(s3d, &scn))) goto error;
  if(RES_OK != (res = s3d_scene_attach_shape(scn, shape))) goto error;
  if(RES_OK != (res = s3d_scene_view_create(scn, 0, &view))) goto error;
  if(RES_OK != (res = s3d_scene_view_compute_volume(view, &volume))) goto error;

exit:
  if(scn) S3D(scene_ref_put(scn));
  if(view) S3D(scene_view_ref_put(view));
  /* The volume may be negative if the faces are not correctly oriented */
  *out_volume = absf(volume);
  return res;
error:
  goto exit;
}

static void
shape_release(struct shape* shape)
{
  ASSERT(shape);
  schiff_mesh_release(&shape->mesh);
  if(shape->shape) S3D(shape_ref_put(shape->shape));
  memset(shape, 0, sizeof(*shape));
}

static res_T
shape_cylinder_init
  (struct shape* shape,
   struct s3d_device* s3d,
   const struct schiff_geometry* geometry)
{
  struct mesh_context ctx;
  struct s3d_vertex_data attrib;
  const struct schiff_cylinder* cylinder;
  double radius, height;
  size_t nverts, nprims;
  res_T res = RES_OK;
  ASSERT(shape && geometry);
  ASSERT(geometry->type == SCHIFF_CYLINDER
      || geometry->type == SCHIFF_CYLINDER_AS_SPHERE);

  memset(shape, 0, sizeof(*shape));
  cylinder = &geometry->data.cylinder;


  res = schiff_mesh_init_cylinder
    (&mem_default_allocator, &shape->mesh, cylinder->nslices);
  if(res != RES_OK) goto error;

  shape->geometry = geometry;

  if(geometry->type == SCHIFF_CYLINDER) /* That's all folks */
    goto exit;

  /* Create the Star-3D cylinder shape */
  res = s3d_shape_create_mesh(s3d, &shape->shape);
  if(res != RES_OK) goto error;

  /* Cylinder parameters must be constant */
  ASSERT(cylinder->radius.distribution == SCHIFF_PARAM_CONSTANT);
  ASSERT(cylinder->height.distribution == SCHIFF_PARAM_CONSTANT);
  radius = cylinder->radius.data.constant;
  height = cylinder->height.data.constant;

  /* Setup the context of the cylinder */
  ctx.type = SCHIFF_CYLINDER;
  ctx.mesh = &shape->mesh;
  ctx.data.cylinder.radius = (float)radius;
  ctx.data.cylinder.height = (float)height;

  /* Define the position vertex attribute */
  attrib.usage = S3D_POSITION;
  attrib.type = S3D_FLOAT3;
  attrib.get = cylinder_get_position;

  nverts = darray_float_size_get(&shape->mesh.vertices.cartesian) / 3/*#coords*/;
  nprims = darray_uint_size_get(&shape->mesh.indices) / 3/*#indices per prim*/;

  res = s3d_mesh_setup_indexed_vertices(shape->shape, (unsigned)nprims,
    geometry_get_indices, (unsigned)nverts, &attrib, 1, &ctx);
  if(res != RES_OK) goto error;

  shape->volume = PI*height*radius*radius; /* Analytic volume */

  schiff_mesh_release(&shape->mesh);

exit:
  return res;
error:
  shape_release(shape);
  goto exit;
}

static res_T
shape_cylinder_generate_s3d_shape
  (const struct shape* shape,
   struct ssp_rng* rng,
   struct s3d_shape* s3d_shape)
{
  struct s3d_vertex_data attrib;
  struct mesh_context ctx;
  size_t nverts, nprims;
  ASSERT(shape && shape->geometry->type == SCHIFF_CYLINDER);

  ctx.type = SCHIFF_CYLINDER;
  ctx.mesh = &shape->mesh;
  ctx.data.cylinder.radius = (float)eval_param
    (&shape->geometry->data.cylinder.radius, rng);
  ctx.data.cylinder.height = (float)eval_param
    (&shape->geometry->data.cylinder.height, rng);

  attrib.usage = S3D_POSITION;
  attrib.type = S3D_FLOAT3;
  attrib.get = cylinder_get_position;

  nverts = darray_float_size_get(&shape->mesh.vertices.cartesian) / 3/*#coords*/;
  nprims = darray_uint_size_get(&shape->mesh.indices) / 3/*#indices per prim*/;

  return s3d_mesh_setup_indexed_vertices(s3d_shape, (unsigned)nprims,
    geometry_get_indices, (unsigned)nverts, &attrib, 1, &ctx);
}

static res_T
shape_cylinder_as_sphere_generate_s3d_shape
  (const struct shape* shape,
   struct ssp_rng* rng,
   struct s3d_shape* s3d_shape)
{
  ASSERT(shape && shape->geometry->type == SCHIFF_CYLINDER_AS_SPHERE);
  (void)rng;
  return s3d_mesh_copy(shape->shape, s3d_shape);
}

static res_T
shape_cylinder_as_sphere_sample_volume_scaling
  (const struct shape* shape,
   struct ssp_rng* rng,
   double* volume_scaling)
{
  double radius, sphere_volume;
  ASSERT(shape && volume_scaling);
  ASSERT(shape->geometry->type == SCHIFF_CYLINDER_AS_SPHERE);

  radius = eval_param(&shape->geometry->data.cylinder.radius_sphere, rng);
  sphere_volume = 4.0/3.0 * PI * radius*radius*radius;
  *volume_scaling = sphere_volume / shape->volume;
  return RES_OK;
}

static res_T
shape_ellipsoid_init
  (struct shape* shape,
   struct s3d_device* s3d,
   const struct schiff_geometry* geometry)
{
  const struct schiff_ellipsoid* ellipsoid;
  struct mesh_context ctx;
  struct s3d_vertex_data attrib;
  double a, b, c, a2, b2, c2;
  size_t nverts, nprims;
  res_T res = RES_OK;
  ASSERT(shape && geometry);
  ASSERT(geometry->type == SCHIFF_ELLIPSOID
      || geometry->type == SCHIFF_ELLIPSOID_AS_SPHERE);

  ellipsoid = &geometry->data.ellipsoid;
  memset(shape, 0, sizeof(*shape));

  res = schiff_mesh_init_sphere_polar
    (&mem_default_allocator, &shape->mesh, ellipsoid->nslices);
  if(res != RES_OK) goto error;

  shape->geometry = geometry;

  if(geometry->type == SCHIFF_ELLIPSOID) /* That's all folks */
    goto exit;

  res = s3d_shape_create_mesh(s3d, &shape->shape);
  if(res != RES_OK) goto error;

  /* Expecting constant parameters */
  ASSERT(ellipsoid->a.distribution == SCHIFF_PARAM_CONSTANT);
  ASSERT(ellipsoid->c.distribution == SCHIFF_PARAM_CONSTANT);
  a = ellipsoid->a.data.constant;
  c = ellipsoid->c.data.constant;
  b = a;
  a2 = a*a;
  b2 = b*b;
  c2 = c*c;

  ctx.mesh = &shape->mesh;
  ctx.type = SCHIFF_ELLIPSOID;
  ctx.data.ellipsoid.abc = a*b*c;
  ctx.data.ellipsoid.b2c2 = b2*c2;
  ctx.data.ellipsoid.a2c2 = a2*c2;
  ctx.data.ellipsoid.a2b2 = a2*b2;

  attrib.usage = S3D_POSITION;
  attrib.type = S3D_FLOAT3;
  attrib.get = ellipsoid_get_position;

  nverts = darray_uint_size_get(&shape->mesh.vertices.polar) / 2/*#theta/phi*/;
  nprims = darray_uint_size_get(&shape->mesh.indices) / 3/*#indices*/;

  res = s3d_mesh_setup_indexed_vertices(shape->shape, (unsigned)nprims,
    geometry_get_indices, (unsigned)nverts, &attrib, 1, &ctx);
  if(res != RES_OK) goto error;

  shape->volume = 4.0/3.0 * PI * ctx.data.ellipsoid.abc;

  schiff_mesh_release(&shape->mesh);

exit:
  return res;
error:
  shape_release(shape);
  goto exit;
}

static res_T
shape_ellipsoid_generate_s3d_shape
  (const struct shape* shape,
   struct ssp_rng* rng,
   struct s3d_shape* s3d_shape)
{
  struct s3d_vertex_data attrib;
  struct mesh_context ctx;
  size_t nverts, nprims;
  double a, b, c, a2, b2, c2;
  ASSERT(shape && shape->geometry->type == SCHIFF_ELLIPSOID);

  ctx.type = SCHIFF_ELLIPSOID;
  ctx.mesh = &shape->mesh;
  a = eval_param(&shape->geometry->data.ellipsoid.a, rng);
  c = eval_param(&shape->geometry->data.ellipsoid.c, rng);

  b = a;
  a2 = a*a;
  b2 = b*b;
  c2 = c*c;

  ctx.mesh = &shape->mesh;
  ctx.type = SCHIFF_ELLIPSOID;
  ctx.data.ellipsoid.abc = a*b*c;
  ctx.data.ellipsoid.b2c2 = b2*c2;
  ctx.data.ellipsoid.a2c2 = a2*c2;
  ctx.data.ellipsoid.a2b2 = a2*b2;

  attrib.usage = S3D_POSITION;
  attrib.type = S3D_FLOAT3;
  attrib.get = ellipsoid_get_position;

  nverts = darray_uint_size_get(&shape->mesh.vertices.polar) / 2/*#theta/phi*/;
  nprims = darray_uint_size_get(&shape->mesh.indices) / 3/*#indices*/;

  return s3d_mesh_setup_indexed_vertices(s3d_shape, (unsigned)nprims,
    geometry_get_indices, (unsigned)nverts, &attrib, 1, &ctx);
}

static res_T
shape_ellipsoid_as_sphere_generate_s3d_shape
  (const struct shape* shape,
   struct ssp_rng* rng,
   struct s3d_shape* s3d_shape)
{
  ASSERT(shape && shape->geometry->type == SCHIFF_ELLIPSOID_AS_SPHERE);
  (void)rng;
  return s3d_mesh_copy(shape->shape, s3d_shape);
}

static res_T
shape_ellipsoid_as_sphere_sample_volume_scaling
  (const struct shape* shape,
   struct ssp_rng* rng,
   double* volume_scaling)
{
  double radius, sphere_volume;
  ASSERT(shape && volume_scaling);
  ASSERT(shape->geometry->type == SCHIFF_ELLIPSOID_AS_SPHERE);

  radius = eval_param(&shape->geometry->data.ellipsoid.radius_sphere, rng);
  sphere_volume = 4.0/3.0 * PI * radius*radius*radius;
  *volume_scaling = sphere_volume / shape->volume;
  return RES_OK;
}

static res_T
shape_helical_pipe_init
  (struct shape* shape,
   struct s3d_device* s3d,
   const struct schiff_geometry* geometry)
{
  const struct schiff_helical_pipe* helical_pipe;
  float* vertices = NULL;
  struct helical_pipe_mesh_context ctx;
  struct s3d_vertex_data attrib;
  size_t nprims, nverts;
  double pitch, height, hradius, cradius;
  res_T res = RES_OK;

  ASSERT(geometry);
  ASSERT(geometry->type == SCHIFF_HELICAL_PIPE
      || geometry->type == SCHIFF_HELICAL_PIPE_AS_SPHERE);

  helical_pipe = &geometry->data.helical_pipe;
  memset(shape, 0, sizeof(*shape));

  res = schiff_mesh_init_helical_pipe(&mem_default_allocator, &shape->mesh,
    helical_pipe->nslices_helicoid, helical_pipe->nslices_circle);
  if(res != RES_OK) goto error;

  shape->geometry = geometry;

  if(geometry->type == SCHIFF_HELICAL_PIPE) /* That's all folks */
    goto exit;

  res = s3d_shape_create_mesh(s3d, &shape->shape);
  if(res != RES_OK) goto error;

  /* Expecting constant parameters */
  ASSERT(helical_pipe->radius_helicoid.distribution == SCHIFF_PARAM_CONSTANT);
  ASSERT(helical_pipe->radius_circle.distribution == SCHIFF_PARAM_CONSTANT);
  ASSERT(helical_pipe->pitch.distribution == SCHIFF_PARAM_CONSTANT);
  ASSERT(helical_pipe->height.distribution == SCHIFF_PARAM_CONSTANT);
  hradius = helical_pipe->radius_helicoid.data.constant;
  cradius = helical_pipe->radius_circle.data.constant;
  pitch = helical_pipe->pitch.data.constant;
  height = helical_pipe->height.data.constant;

  /* Setup the mesh vertices */
  res = schiff_mesh_helical_pipe_create_vertices(&shape->mesh, pitch, height,
    hradius, cradius, helical_pipe->nslices_helicoid,
    helical_pipe->nslices_circle, &nverts, &vertices);
  if(res != RES_OK) return res;

  ctx.vertices = vertices;
  ctx.indices = darray_uint_cdata_get(&shape->mesh.indices);

  attrib.usage = S3D_POSITION;
  attrib.type = S3D_FLOAT3;
  attrib.get = helical_pipe_get_position;

  nprims = darray_uint_size_get(&shape->mesh.indices) / 3/*#indices per prim*/;

  res = s3d_mesh_setup_indexed_vertices(shape->shape, (unsigned)nprims,
    helical_pipe_get_indices, (unsigned)nverts, &attrib, 1, &ctx);
  if(res != RES_OK) goto error;

  res = compute_s3d_shape_volume(s3d, shape->shape, &shape->volume);
  if(res != RES_OK) goto error;

  schiff_mesh_release(&shape->mesh);

exit:
  if(vertices) {
    schiff_mesh_helical_pipe_destroy_vertices(&shape->mesh, vertices);
  }
  return res;
error:
  shape_release(shape);
  goto exit;
}

static res_T
shape_helical_pipe_generate_s3d_shape
  (struct shape* shape,
   struct ssp_rng* rng,
   struct s3d_shape* s3d_shape)
{
  const struct schiff_helical_pipe* helical_pipe;
  float* vertices = NULL;
  struct helical_pipe_mesh_context ctx;
  struct s3d_vertex_data attrib;
  size_t nprims, nverts;
  double pitch, height, hradius, cradius;
  res_T res = RES_OK;
  ASSERT(shape && shape->geometry->type == SCHIFF_HELICAL_PIPE);

  helical_pipe = &shape->geometry->data.helical_pipe;

  /* Sample the helical pipe attributes */
  hradius = eval_param(&helical_pipe->radius_helicoid, rng);
  cradius = eval_param(&helical_pipe->radius_circle, rng);
  pitch = eval_param(&helical_pipe->pitch, rng);
  height = eval_param(&helical_pipe->height, rng);

  /* Setup the mesh vertices */
  res = schiff_mesh_helical_pipe_create_vertices(&shape->mesh, pitch, height,
    hradius, cradius, helical_pipe->nslices_helicoid,
    helical_pipe->nslices_circle, &nverts, &vertices);
  if(res != RES_OK) goto error;

  ctx.vertices = vertices;
  ctx.indices = darray_uint_cdata_get(&shape->mesh.indices);

  attrib.usage = S3D_POSITION;
  attrib.type = S3D_FLOAT3;
  attrib.get = helical_pipe_get_position;

  nprims = darray_uint_size_get(&shape->mesh.indices) / 3/*#indices per prim*/;

  res = s3d_mesh_setup_indexed_vertices(s3d_shape, (unsigned)nprims,
    helical_pipe_get_indices, (unsigned)nverts, &attrib, 1, &ctx);
  if(res != RES_OK) goto error;

exit:
  if(vertices) {
    schiff_mesh_helical_pipe_destroy_vertices(&shape->mesh, vertices);
  }
  return res;
error:
  goto exit;
}

static res_T
shape_helical_pipe_as_sphere_generate_s3d_shape
  (struct shape* shape,
   struct ssp_rng* rng,
   struct s3d_shape* s3d_shape)
{
  ASSERT(shape && shape->geometry->type == SCHIFF_HELICAL_PIPE_AS_SPHERE);
  (void)rng;
  return s3d_mesh_copy(shape->shape, s3d_shape);
}

static res_T
shape_helical_pipe_as_sphere_sample_volume_scaling
  (const struct shape* shape,
   struct ssp_rng* rng,
   double* volume_scaling)
{
  double radius, sphere_volume;
  ASSERT(shape && volume_scaling);
  ASSERT(shape->geometry->type == SCHIFF_HELICAL_PIPE_AS_SPHERE);

  radius = eval_param(&shape->geometry->data.helical_pipe.radius_sphere, rng);
  sphere_volume = 4.0/3.0 * PI * radius*radius*radius;
  *volume_scaling = sphere_volume / shape->volume;
  return RES_OK;
}

static res_T
shape_sphere_init
  (struct shape* shape,
   struct s3d_device* s3d,
   const struct schiff_geometry* geometry)
{
  struct mesh_context ctx;
  struct s3d_vertex_data attrib;
  const struct schiff_sphere* sphere;
  size_t nverts, nprims;
  res_T res = RES_OK;
  ASSERT(shape && geometry && geometry->type == SCHIFF_SPHERE);

  sphere = &geometry->data.sphere;
  memset(shape, 0, sizeof(*shape));

  /* Generate the sphere mesh */
  res = schiff_mesh_init_sphere
    (&mem_default_allocator, &shape->mesh, sphere->nslices);
  if(res != RES_OK) goto error;

  shape->geometry = geometry;

  /* Create the Star-3D sphere shape */
  res = s3d_shape_create_mesh(s3d, &shape->shape);
  if(res != RES_OK) goto error;

  /* Setup the context of the sphere*/
  ctx.type = SCHIFF_SPHERE;
  ctx.mesh = &shape->mesh;
  ctx.data.sphere.radius = 1.f;

  /* Define the position vertex attribute */
  attrib.usage = S3D_POSITION;
  attrib.type = S3D_FLOAT3;
  attrib.get = sphere_get_position;

  nverts = darray_float_size_get(&shape->mesh.vertices.cartesian) / 3/*#coords*/;
  nprims = darray_uint_size_get(&shape->mesh.indices) / 3/*#indices*/;

  res = s3d_mesh_setup_indexed_vertices(shape->shape, (unsigned)nprims,
    geometry_get_indices, (unsigned)nverts, &attrib, 1, &ctx);
  if(res != RES_OK) goto error;

  shape->volume = 4.0/3.0*PI; /* Analytic volume. The radius is implicitly 1 */

exit:
  schiff_mesh_release(&shape->mesh);
  return res;
error:
  shape_release(shape);
  goto exit;
}

static res_T
shape_sphere_generate_s3d_shape
  (const struct shape* shape,
   struct ssp_rng* rng,
   struct s3d_shape* s3d_shape)
{
  ASSERT(shape && shape->geometry->type == SCHIFF_SPHERE);
  (void)rng;
  return s3d_mesh_copy(shape->shape, s3d_shape);
}

static res_T
shape_sphere_sample_volume_scaling
  (const struct shape* shape,
   struct ssp_rng* rng,
   double* volume_scaling)
{
  double radius, sphere_volume;
  ASSERT(shape && volume_scaling);
  ASSERT(shape->geometry->type == SCHIFF_SPHERE);

  radius = eval_param(&shape->geometry->data.sphere.radius, rng);
  sphere_volume = 4.0/3.0 * PI * radius*radius*radius;
  *volume_scaling = sphere_volume / shape->volume;
  return RES_OK;
}

static res_T
shape_supershape_init
  (struct shape* shape,
   struct s3d_device* s3d,
   const struct schiff_geometry* geometry)
{
  const struct schiff_supershape* sshape;
  struct mesh_context ctx;
  struct s3d_vertex_data attrib;
  size_t nverts, nprims;
  int iform, iattr;
  res_T res = RES_OK;
  ASSERT(geometry);
  ASSERT(geometry->type == SCHIFF_SUPERSHAPE
      || geometry->type == SCHIFF_SUPERSHAPE_AS_SPHERE);

  sshape = &geometry->data.supershape;
  memset(shape, 0, sizeof(*shape));

  res = schiff_mesh_init_sphere_polar(&mem_default_allocator,
    &shape->mesh, geometry->data.supershape.nslices);
  if(res != RES_OK) goto error;

  shape->geometry = geometry;

  if(geometry->type == SCHIFF_SUPERSHAPE) /* That's all folks */
    goto exit;

  res = s3d_shape_create_mesh(s3d, &shape->shape);
  if(res != RES_OK) goto error;

  ctx.type = SCHIFF_SUPERSHAPE;
  ctx.mesh = &shape->mesh;
  FOR_EACH(iform, 0, 2) {
  FOR_EACH(iattr, 0, 6) {
    /* Expecting constant parameters */
    ASSERT(sshape->formulas[iform][iattr].distribution == SCHIFF_PARAM_CONSTANT);
    ctx.data.supershape.formulas[iform][iattr] =
      sshape->formulas[iform][iattr].data.constant;
  }}

  attrib.usage = S3D_POSITION;
  attrib.type = S3D_FLOAT3;
  attrib.get = supershape_get_position;

  nverts = darray_uint_size_get(&shape->mesh.vertices.polar) / 2/*#theta/phi*/;
  nprims = darray_uint_size_get(&shape->mesh.indices) / 3/*#indices*/;

  res = s3d_mesh_setup_indexed_vertices(shape->shape, (unsigned)nprims,
    geometry_get_indices, (unsigned)nverts, &attrib, 1, &ctx);
  if(res != RES_OK) goto error;

  res = compute_s3d_shape_volume(s3d, shape->shape, &shape->volume);
  if(res != RES_OK) goto error;

  schiff_mesh_release(&shape->mesh);

exit:
  return res;
error:
  shape_release(shape);
  goto exit;
}

static res_T
shape_supershape_generate_s3d_shape
  (const struct shape* shape,
   struct ssp_rng* rng,
   struct s3d_shape* s3d_shape)
{
  const struct schiff_supershape* sshape;
  struct mesh_context ctx;
  struct s3d_vertex_data attrib;
  size_t nverts, nprims;
  int iform, iattr;
  ASSERT(shape && shape->geometry->type == SCHIFF_SUPERSHAPE);

  sshape = &shape->geometry->data.supershape;
  ctx.mesh = &shape->mesh;
  ctx.type = SCHIFF_SUPERSHAPE;

  FOR_EACH(iform, 0, 2) {
  FOR_EACH(iattr, 0, 6) {
    ctx.data.supershape.formulas[iform][iattr] =
      (float)eval_param(&sshape->formulas[iform][iattr], rng);
  }}

  attrib.usage = S3D_POSITION;
  attrib.type = S3D_FLOAT3;
  attrib.get = supershape_get_position;

  nverts = darray_uint_size_get(&shape->mesh.vertices.polar) / 2/*#theta/phi*/;
  nprims = darray_uint_size_get(&shape->mesh.indices) / 3/*#indices*/;

  return s3d_mesh_setup_indexed_vertices(s3d_shape, (unsigned)nprims,
    geometry_get_indices, (unsigned)nverts, &attrib, 1, &ctx);
}

static res_T
shape_supershape_as_sphere_generate_s3d_shape
  (const struct shape* shape,
   struct ssp_rng* rng,
   struct s3d_shape* s3d_shape)
{
  ASSERT(shape && shape->geometry->type == SCHIFF_SUPERSHAPE_AS_SPHERE);
  (void)rng;
  return s3d_mesh_copy(shape->shape, s3d_shape);
}

static res_T
shape_supershape_as_sphere_sample_volume_scaling
  (const struct shape* shape,
   struct ssp_rng* rng,
   double* volume_scaling)
{
  double radius, sphere_volume;
  ASSERT(shape && volume_scaling);
  ASSERT(shape->geometry->type == SCHIFF_SUPERSHAPE_AS_SPHERE);

  radius = eval_param(&shape->geometry->data.supershape.radius_sphere, rng);
  sphere_volume = 4.0/3.0 * PI * radius*radius*radius;
  *volume_scaling = sphere_volume / shape->volume;
  return RES_OK;
}

static res_T
geometry_sample_shape
  (struct ssp_rng* rng,
   void** shape_data,
   struct s3d_shape* s3d_shape,
   void* ctx)
{
  struct geometry_distribution_context* distrib = ctx;
  struct shape* shape;
  size_t isamp;
  res_T res = RES_OK;
  ASSERT(rng && shape_data && ctx);

  isamp = ssp_ranst_discrete_get(rng, distrib->ran_geometries);
  shape = distrib->shapes + isamp;

  switch(shape->geometry->type) {
    case SCHIFF_ELLIPSOID:
      res = shape_ellipsoid_generate_s3d_shape(shape, rng, s3d_shape);
      break;
    case SCHIFF_ELLIPSOID_AS_SPHERE:
      res = shape_ellipsoid_as_sphere_generate_s3d_shape(shape, rng, s3d_shape);
      break;
    case SCHIFF_CYLINDER:
      res = shape_cylinder_generate_s3d_shape(shape, rng, s3d_shape);
      break;
    case SCHIFF_CYLINDER_AS_SPHERE:
      res = shape_cylinder_as_sphere_generate_s3d_shape(shape, rng, s3d_shape);
      break;
    case SCHIFF_HELICAL_PIPE:
      res = shape_helical_pipe_generate_s3d_shape(shape, rng, s3d_shape);
      break;
    case SCHIFF_HELICAL_PIPE_AS_SPHERE:
      res = shape_helical_pipe_as_sphere_generate_s3d_shape(shape, rng, s3d_shape);
      break;
    case SCHIFF_SPHERE:
      res = shape_sphere_generate_s3d_shape(shape, rng, s3d_shape);
      break;
    case SCHIFF_SUPERSHAPE:
      res = shape_supershape_generate_s3d_shape(shape, rng, s3d_shape);
      break;
    case SCHIFF_SUPERSHAPE_AS_SPHERE:
      res = shape_supershape_as_sphere_generate_s3d_shape(shape, rng, s3d_shape);
      break;
    default: FATAL("Unreachable code.\n"); break;
  }
  if(res != RES_OK) goto error;

exit:
  *shape_data = shape;
  return res;
error:
  goto exit;
}

static res_T
geometry_sample_volume_scaling
  (struct ssp_rng* rng,
   void* shape_data,
   double* volume_scaling,
   void* ctx)
{
  struct shape* shape = shape_data;
  res_T res = RES_OK;
  ASSERT(rng && shape_data && volume_scaling && ctx);
  (void)ctx;

  switch(shape->geometry->type) {
    case SCHIFF_ELLIPSOID:
    case SCHIFF_CYLINDER:
    case SCHIFF_HELICAL_PIPE:
    case SCHIFF_SUPERSHAPE:
      *volume_scaling = 1;
      break;
    case SCHIFF_ELLIPSOID_AS_SPHERE:
      res = shape_ellipsoid_as_sphere_sample_volume_scaling
        (shape, rng, volume_scaling);
      break;
    case SCHIFF_CYLINDER_AS_SPHERE:
      res = shape_cylinder_as_sphere_sample_volume_scaling
        (shape, rng, volume_scaling);
      break;
    case SCHIFF_HELICAL_PIPE_AS_SPHERE:
      res = shape_helical_pipe_as_sphere_sample_volume_scaling
        (shape, rng, volume_scaling);
      break;
    case SCHIFF_SPHERE:
      res = shape_sphere_sample_volume_scaling
        (shape, rng, volume_scaling);
      break;
    case SCHIFF_SUPERSHAPE_AS_SPHERE:
      res = shape_supershape_as_sphere_sample_volume_scaling
        (shape, rng, volume_scaling);
      break;
    default: FATAL("Unreachable code.\n"); break;
  }
  if(res != RES_OK) goto error;
  ASSERT(*volume_scaling > 0);

exit:
  return res;
error:
  goto exit;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
void
schiff_geometry_distribution_release
  (struct sschiff_geometry_distribution* distrib)
{
  struct geometry_distribution_context* ctx;
  size_t i, count;
  ASSERT(distrib);
  if(!distrib->context) return;

  ctx = distrib->context;
  if(ctx->shapes) {
    count = sa_size(ctx->shapes);
    FOR_EACH(i, 0, count)
      shape_release(&ctx->shapes[i]);
    sa_release(ctx->shapes);
  }
  if(ctx->ran_geometries)
    SSP(ranst_discrete_ref_put(ctx->ran_geometries));
  mem_rm(ctx);
}

res_T
schiff_geometry_distribution_init
  (struct sschiff_geometry_distribution* distrib,
   struct s3d_device* s3d,
   const struct schiff_geometry* geoms,
   const size_t ngeoms,
   const double characteristic_length,
   struct ssp_ranst_discrete* ran_geoms,
   struct schiff_optical_properties* properties)
{
  struct geometry_distribution_context* ctx = NULL;
  size_t i;
  res_T res = RES_OK;
  ASSERT(distrib && geoms && ngeoms && ran_geoms);
  /* Note that properties may be NULL if the "-G" option is defined */

  *distrib = SSCHIFF_NULL_GEOMETRY_DISTRIBUTION;

  ctx = mem_calloc(1, sizeof(struct geometry_distribution_context));
  if(!ctx) {
    fprintf(stderr,
      "Couldn't allocate the geometry distribution context\n");
    res = RES_MEM_ERR;
    goto error;
  }
  /* Directly setup the distribution context to avoid memory leaks on error */
  distrib->context = ctx;

  if(!sa_add(ctx->shapes, ngeoms)) {
    fprintf(stderr,
      "Couldn't allocate the shapes of the geometry distributions.\n");
    res = RES_MEM_ERR;
    goto error;
  }
  memset(ctx->shapes, 0, sizeof(ctx->shapes[0])*ngeoms);

  FOR_EACH(i, 0, ngeoms) {
    /* Generate the mesh template and setup its distribution context */
    switch(geoms[i].type) {
      case SCHIFF_ELLIPSOID:
      case SCHIFF_ELLIPSOID_AS_SPHERE:
        res = shape_ellipsoid_init(&ctx->shapes[i], s3d, &geoms[i]);
        break;
      case SCHIFF_CYLINDER:
      case SCHIFF_CYLINDER_AS_SPHERE:
        res = shape_cylinder_init(&ctx->shapes[i], s3d, &geoms[i]);
        break;
      case SCHIFF_HELICAL_PIPE:
      case SCHIFF_HELICAL_PIPE_AS_SPHERE:
        res = shape_helical_pipe_init(&ctx->shapes[i], s3d, &geoms[i]);
        break;
      case SCHIFF_SPHERE:
        res = shape_sphere_init(&ctx->shapes[i], s3d, &geoms[i]);
        break;
      case SCHIFF_SUPERSHAPE:
      case SCHIFF_SUPERSHAPE_AS_SPHERE:
        res = shape_supershape_init(&ctx->shapes[i], s3d, &geoms[i]);
        break;
      default: FATAL("Unreachable code.\n"); break;
    }
    if(res != RES_OK) {
      fprintf(stderr,
        "Couldn't initialise the shape of the geometry distribution.\n");
      goto error;
    }
  }
  ctx->properties = properties;
  SSP(ranst_discrete_ref_get(ran_geoms));
  ctx->ran_geometries = ran_geoms;

  distrib->material.get_property = get_material_property;
  distrib->material.material = properties;
  distrib->sample = geometry_sample_shape;
  distrib->sample_volume_scaling = geometry_sample_volume_scaling;
  distrib->characteristic_length = characteristic_length;

exit:
  return res;
error:
  schiff_geometry_distribution_release(distrib);
  goto exit;
}

