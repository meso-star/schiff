/* Copyright (C) 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015, 2016 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SCHIFF_OPTICAL_PROPERTIES_H
#define SCHIFF_OPTICAL_PROPERTIES_H

#include <rsys/dynamic_array.h>
#include <rsys/rsys.h>

struct sschiff_material_properties;

struct schiff_optical_properties {
  double W; /* Wavelength */
  double Nr; /* Real part of the relative refractive index */
  double Kr; /* Imaginary part of the relative refractive index */
  double Ne; /* Refractive index of the medium */
};

static INLINE char
schiff_optical_properties_check
  (const struct schiff_optical_properties* props)
{
  ASSERT(props);
  return props->W  >= 0.0
      && props->Nr >= 0.0
      && props->Kr >= 0.0
      && props->Ne >= 0.0;
}

extern LOCAL_SYM res_T
schiff_optical_properties_load
  (struct schiff_optical_properties** props, /* Stretchy array */
   const char* filename);

extern LOCAL_SYM res_T
schiff_optical_properties_load_stream
  (struct schiff_optical_properties** props, /* Stretchy array */
   FILE* stream,
   const char* stream_name);

extern LOCAL_SYM void
schiff_optical_properties_fetch
  (struct schiff_optical_properties* properties, /* Stetchy array */
   const double wavelength,
   struct sschiff_material_properties* props);

#endif /* SCHIFF_OPTICAL_PROPERTIES_H */

