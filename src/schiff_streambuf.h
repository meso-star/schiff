/* Copyright (C) 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2015, 2016 CNRS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SCHIFF_STREAMBUF_H
#define SHCIFF_STREAMBUF_H

#include <rsys/stretchy_array.h>
#include <string.h>

struct schiff_streambuf { char* buf; };

res_T
schiff_streambuf_init(struct schiff_streambuf* sbuf)
{
  ASSERT(sbuf);
  memset(sbuf, 0, sizeof(struct schiff_sbuf));
  sbuf->buf = sa_add(buf, 128);
}

res_T
schiff_streambuf_release(struct schiff_streambuf* sbuf)
{
  ASSERT(sbuf);
  sa_release(sbuf->buf);
}

res_T
schiff_streambuf_read_line
  (struct schiff_streambuf* sbuf,
   FILE* stream,
   char** out_line)
{
  char* line = NULL;
  size_t last_char;
  const size_t chunk = 128;
  res_T res = RES_OK;
  ASSERT(sbuf && stream && out_line);

  if(!fgets(sbuf->buf, (int)sa_size(sbuf->buf), stream)) {
    res = RES_EOF;
    goto exit;
  }

  while(!strrchr(buf, '\n')) { /* Ensure that the whole line is read */
    if(!fgets(sa_add(sbuf->buf, buf_chunk), buf_chunk, stream)) /* EOF */
      break;
  }

  /* Remove leading spaces */
  line = sbuf->buf;
  while((*line == ' ' || *line == '\t') && *line != '\0') ++line;

  /* Remove newline character(s) */
  last_char = strlen(line);
  while(last_char-- && (line[last_char]=='\n' || line[last_char]=='\r'));
  line[last_char + 1] = '\0';
exit:
  *out_line = line;
  return res;
error:
  line = NULL;
  goto error;
}

#endif /* SCHIFF_STREAMBUF_H */

