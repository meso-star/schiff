.\" Copying and distribution of this file, with or without modification,
.\" are permitted in any medium without royalty provided the copyright
.\" notice and this notice are preserved. This file is offered as-is,
.\" without any warranty.
.TH SCHIFF-OUTPUT 5
.SH NAME
schiff\-output \- format of
.BR schiff (1)
results.
.SH DESCRIPTION
The output result of the
.BR schiff (1)
program is a collection of ASCII floating point data. Each set of floating
point values are separated by an empty line. The first set is a list of per
wavelength cross\-sections. Each line stores the estimated cross\-section for a
wavelength submitted by the \fB\-w\fR option of
.BR schiff (1).
It is formatted as "W E e A a S s P p" with "W" the wavelength in vacuum
(expressed in microns), "E", "A" and "S" the estimation of the extinction,
absorption and scattering cross\-sections, respectively, in square microns per
particle, and "P" the estimated average projected area of the soft particles
expressed in square microns per particle. The "e", "a", "s" and "p" values are
the standard error of the aforementioned estimations.
.PP
Following the list of cross\-sections comes the list of phase function
descriptors. Each descriptor is a line that gives informations on the
[[inverse] cumulative] phase functions. It is formatted as "W theta-l Ws Ws-SE
Wc Wc-SE n nangles nangles\-inv" with "W" the wavelength in vacuum (expressed in
microns) of the inverse cumulative phase function, "theta-l" the scattering
angle in radians from which the phase function was analytically computed, "Ws"
and "Wc" the values of the differential cross\-section and its cumulative at
"theta-l", "n" the parameter of the model used to analytically evaluate the
phase function for large scattering angles (i.e. angles greater than "theta-l"),
"nangles" the number of scattering angles (\fB\-a\fR option of
.BR schiff (1))
and "nangles\-inv" the number of inverse cumulative phase function values
(\fB\-A\fR option of
.BR schiff (1)).
The "Ws-SE" and "Wc-SE" values are the standard error of the "Ws" and "Wc"
estimations, respectively.
.PP
Then there is the list of phase functions, each stored as a list of lines
formatted as "A E SE" where "E" is the expected value of the phase function for
the input scattering angle "A" in radians, and "SE" its standard error.  The
number of scattering angles is controlled by the \fB\-a\fR option of
.BR schiff (1).
.PP
After the phase functions come the cumulative phase functions that follow the
format of the phase functions, i.e. each cumulative phase function is a list a
lines \- one per scattering angle \- that defines the input scattering angle in
radians, followed by the expected value and the standard error of its cumulative
phase function.
.PP
Finally, there is the inverse cumulative phase functions. Each of these
functions lists a set of N probabilities in [0, 1] and its corresponding
scattering angles in [0, PI]. The number of entries of the inverse cumulative
phase functions is controlled by the \fB\-A\fR option of
.BR schiff (1).
Assuming a set of N angles, the i^th angle (i in [0, N\-1]) is the angle whose
probability is i/(N\-1).
.PP
Note that the cross sections, the phase function descriptors, the phase
functions, their cumulative and their inverse cumulative are all sorted in
ascending order with respect to their associated wavelength.
.SH GRAMMAR
The following grammar formally describes the
.BR schiff (1)
output format.
The output values are ASCII data formatted line by line. By convention, in the
following grammar the line data are listed between quote marks. The grammar may
use new lines for formatting constraints, but data are actually on the same
line while a closed quote mark is not defined.
.PP
.RS 4
.nf
<schiff\-output>           ::= <cross\-sections>
                              EMPTY\-LINE
                              <phase\-function\-descriptor>
                              EMPTY\-LINE
                              <phase\-functions>
                              EMPTY-LINE
                              <cumulative\-phase\-functions>
                              EMPTY\-LINE
                              <inverse\-cumulative\-phase\-functions>
                              EMPTY\-LINE

\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-

<cross\-sections>         ::= "WAVELENGTH <extinction> <absorption> 
                              <scattering> <area>"
                           [ <cross\-sections> ]

<extinction>             ::= ESTIMATION STANDARD\-ERROR
<absorption>             ::= ESTIMATION STANDARD\-ERROR
<scattering>             ::= ESTIMATION STANDARD\-ERROR
<area>                   ::= ESTIMATION STANDARD\-ERROR

\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-

<phase\-functions\-descriptors> 
                         ::= "WAVELENGTH THETA <PF(THETA)> <CDF(THETA)> 
                              N #ANGLES #INVCUM"
                           [ <phase\-functions\-descriptors> ]

<CDF(THETA)>             ::= ESTIMATION STANDARD\-ERROR
<PF(THETA)>              ::= ESTIMATION STANDARD\-ERROR

\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-

<phase\-functions>        ::= <function\-entries>
                           [ EMPTY-LINE 
                             <phase\-functions> ]

<cumulative\-phase\-functions> 
                         ::= <function\-entries>
                           [ EMPTY-LINE
                             <cumulative\-phase\-functions> ]

<function\-entries>       ::= ANGLE ESTIMATION STANDARD-ERROR
                           [ <phase\-function\-entries> ]

<inverse\-cumulative\-phase\-functions> 
                         ::= <inverse\-function\-entries>
                           [ EMPTY-LINE
                             <inverse\-cumulative\-phase\-functions> ]

<inverse\-function\-entries> 
                         ::= PROBABILITY ANGLE
                           [ <inverse\-function\-entries> ]
.fi
.SH EXAMPLE
The following output is emitted by the
.BR schiff (1)
program invoked on the wavelengths 0.3 and 0.6 micron.
Note that actually,
.BR schiff (1)
does not write comments, i.e. text preceeded by the "#" character. However
comments are added in order to help in understanding the data layout.
.RS 4
.PP
.nf
0.3 10.61 0.20 9.51e-3 2.37e-4 10.6 0.20 5.25 0.10 \fB# X\-sections\fR
0.6 11.15 0.25 4.76e-3 1.19e-4 11.1 0.25 5.25 0.10 \fB# X\-sections\fR
0.3 0.18 1.37 17.6 7.74 0.73 0.80 1000 2000 \fB# descriptor\fR
0.6 0.26 9.81 5.26 7.65 0.48 2.90 1000 2000 \fB# descriptor\fR
0 520.23 64.2971 \fB# Phase function (0.3 micron)\fR
0.00314474 474.315 50.6471
    ...
3.13845 0.0196258 0
3.14159 0.0196259 0
.PP
0 150.183 25.4822 \fB# Phase function (0.6 micron)\fR
0.00314474 145.969 23.7955
    ...
3.13845 0.00262338 0
3.14159 0.00262338 0
.PP
0 0 0 \fB# Cumulative (0.3 micron)\fR
0.00314474 0.0154297 0.00177366
    ...
3.13845 0.999999 0
3.14159 1 0
.PP
0 0 0 \fB# Cumulative (0.6 micron)\fR
0.00314474 0.00460001 0.000765182
    ...
3.13845 1 0
3.14159 1 0
.PP
0 0 \fB# Inverse cumulative (0.3 micron)\fR
0.00050025 0.000101956
    ...
0.9995 3.05143
1 3.14159
.PP
0 0 \fB# Inverse cumulative (0.6 micron)\fR
0.00050025 0.00034199
    ...
0.9995 2.89409
1 3.14159
.fi
.SH SEE ALSO
.BR schiff (1)
