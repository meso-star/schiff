.\" Copying and distribution of this file, with or without modification,
.\" are permitted in any medium without royalty provided the copyright
.\" notice and this notice are preserved. This file is offered as-is,
.\" without any warranty.
.TH SCHIFF-GEOMETRY 5
.SH NAME
schiff-geometry \- control the shape of soft particles
.SH DESCRIPTION
\fBschiff-geometry\fR is a YAML file [1] that controls the geometry
distribution of soft particles. The
.BR schiff (1)
program relies on this description to generate the shape of the sampled soft
particles.
.PP
A geometry is defined by a type and a set of parameters whose value is
controlled by a distribution. Several geometries with their own probability can
be declared in the same \fBschiff-geometry\fR file to define a discrete random
variate of geometries. This allow to finely tune the shapes of the soft
particles with a collection of geometries, each representing a specific sub-set
of shapes of the soft particles to handle.
.SH GRAMMAR
This section describes the \fBschiff\-geometry\fR grammar based on the YAML
human readable data format [1]. The YAML format provides several ways to define
a mapping or a sequence of data. The following grammar always uses the more
verbose form but any alternative YAML formatting can be used instead. Refer to
the example section for illustrations of such alternatives.
.PP
When the \fBradius_sphere\fR optional parameter is defined, the relative shape
of the geometry must be fixed, i.e. all other parameters must be constants. In
this situation, only the volume of the geometry is variable; it is equal to the
volume of an equivalent sphere whose radius is controlled by the distribution
of the \fBradius_sphere\fR parameter.
.PP
The \fBslices\fR optional attribute controls the discretization of the
geometries in triangular meshes, i.e. the number of discrete steps around 2PI.
When not defined it is assumed to be 64. Note that the \fBhelical_pipe\fR
geometry exposes 2 discretization parameters: \fBslices_circle\fR and
\fBslices_helicoid\fR. The former controls the discretization of the meridian
around 2PI while the later defines the total number of discrete steps along the
helicoid curve. When not defined \fBslices_circle\fR and \fBslices_helicoid\fR
are set to 64 and 128, respectively.
.PP
All the geometries have the \fBproba\fR optional attribute that defines the
unnormalized probability to sample the geometry. If it is not defined, it is
assumed to be equal to 1.
.PP
.RS 4
.nf
<schiff\-geometry>        ::= <geometry> | <geometry\-list>

<geometry\-list>          ::= \- <geometry>
                           [ \- <geometry> ]

<geometry>               ::= <cylinder\-geometry>
                           | <ellipsoid\-geometry>
                           | <helical\-pipe\-geometry>
                           | <sphere\-geometry>
                           | <supershape\-geometry>

<cylinder\-geometry>      ::= cylinder:
                               radius: <distribution>
                               height: <distribution>
                           [   radius_sphere: <distribution> ]
                           [   slices: INTEGER ]
                           [   proba: REAL ]

<ellipsoid\-geometry>     ::= ellipsoid:
                               a: <distribution>
                               c: <distribution>
                           [   radius_sphere: <distribution> ]
                           [   slices: INTEGER ]
                           [   proba: REAL ]

<helical\-pipe\-geometry>  ::= helical_pipe:
                               pitch: <distribution>
                               height: <distribution>
                               radius_helicoid: <distribution>
                               radius_circle: <distribution>
                           [   radius_sphere: <distribution> ]
                           [   slices_helicoid: INTEGER ]
                           [   slices_circle: INTEGER ]

<sphere\-geometry>        ::= sphere:
                               radius: <distribution>
                           [   slices: INTEGER ]
                           [   proba: REAL ]

<supershape\-geometry>    ::= supershape:
                               formula0: <superformula>
                               formula1: <superformula>
                           [   radius_sphere: <distribution> ]
                           [   slices: INTEGER ]
                           [   proba: REAL ]

<superformula>           ::= A: <distribution>
                             B: <Idistribution>
                             M: <distribution>
                             N0: <distribution>
                             N1: <distribution>
                             N2: <distribution>

\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-

<distribution>           ::= <constant>
                           | <gaussian>
                           | <histogram>
                           | <lognormal>

<constant>               ::= REAL

<lognormal>              ::= lognormal:
                               mu: REAL
                               sigma: REAL

<gaussian>               ::= gaussian:
                               mu: REAL
                               sigma: REAL

<histogram>              ::= histogram:
                               lower: REAL
                               upper: REAL
                               probabilities:
                                 <probabilities\-list>

<probabilities\-list>     ::= \- REAL
                           [ \- <probabilities\-list> ]
.fi
.SH GEOMETRY TYPES
.PP
\fBcylinder\fR
.PP
.RS 4
A cylinder is simply defined by its \fBheight\fR and a its \fBradius\fR.
.RE
.PP
\fBellipsoid\fR
.PP
.RS 4
The shape of an ellipsoid geometry is controlled by the length of its
semi\-principal axises \fBa\fR and \fBc\fR used to evaluate the following
equation:
.PP
.RS 8
.nf
(x/\fBa\fR)^2 + (y/\fBa\fR)^2 + (z/\fBc\fR)^2 = 1
.fi
.RE
.RE
.PP
\fBhelical_pipe\fR
.RS 4
.PP
Helicoid whose meridian shape is a circle that is orthogonal to the helicoid
slope. Its \fBpitch\fR defines the width of a complete helicoid turn and its
\fBheight\fR controls the overall distance between the beginning and the end of
the helicoid. Finally, the \fBradius_helicoid\fR and the \fBradius_circle\fR
defines the radius of the helicoid and the radius of its meridian,
respectively. Let "u" in [0, \fBheight\fR * 2PI / \fBpitch\fR] and "t" in [0,
2PI], the "X", "Y" and "Z" 3D coordinates of the helicoid points are computed
from the following equations:
.PP
.RS 8
.nf
X(t, u) = x(t)*cos(u) - y(t)*sin(u)
Y(t, u) = x(t)*sin(u) + y(t)*cos(u)
Z(t, u) = z(t) + c*u
.PP
x(t) = \fBradius_helicoid\fR + \fBradius_circle\fR*cos(t)
y(t) = -\fBradius_circle\fR * c / A * sin(t)
z(t) = \fBradius_circle\fR*\fBradius_helicoid\fR / A * sin(t)
.PP
c = \fBpitch\fR / 2PI
A = sqrt(\fBradius_helicoid\fR^2 + c^2)
.fi
.RE
.RE
.PP
\fBsphere\fR
.RS 4
.PP
A sphere is simply defined by its \fBradius\fR.
.RE
.PP
\fBsupershape\fR
.RS 4
.PP
Generalisation of the superellipsoid that is well suited to represent many
complex shapes found in the nature. It is controlled by 2 superformulas, each
defining a radius "r" for a given angle "a":
.PP
.RS 8
.nf
r(a) = ( |cos(\fBM\fR*a/4)/\fBA\fR)|^\fBN1\fR + |sin(\fBM\fR*a/4)/\fBB\fR|^\fBN2\fR )^{-1/\fBN0\fR}
.fi
.RE
.PP
Assuming a point with the spherical coordinates {theta, phi}, the corresponding
3D coordinates onto the supershape is obtained by evaluating the following
relations:
.RS 8
.PP
.nf
x = r0(theta)*cos(theta) * r1(phi)*cos(phi)
y = r0(theta)*sin(theta) * r1(phi)*cos(phi)
z = r1(phi)*sin(phi)
.fi
.SH PARAMETER DISTRIBUTIONS
.PP
\fBconstant\fR
.RS 4
.PP
Fixe the value of the parameter.
.RE
.PP
\fBgaussian\fR
.RS 4
.PP
Use the following probability distribution to define the parameter according to
the mean value \fBmu\fR and the standard deviation \fBsigma\fR:
.PP
.RS 8
.nf
P(x) dx = 1 / (\fBsigma\fR*sqrt(2*PI)) * exp(1/2*((x-\fBmu\fR)/\fBsigma\fR)^2) dx
.fi
.RE
.RE
.PP
\fBhistogram\fR
.RS 4
.PP
Split the parameter domain [\fBlower\fR, \fBupper\fR] in \fIN\fR intervals of
length (\fBupper\fR-\fBlower\fR)/\fIN\fR.  The list of unnormalized
probabilities of the interval bounds are listed in the \fBprobabilities\fR
array and are used to build the cumulative distribution of the parameter. Let a
random number "r" in [0, 1], the corresponding parameter value is computed by
retrieving the interval of the parameter from the aforementioned cumulative,
before linearly interpolating its bounds with respect to "r";
.RE
.PP
\fBlognormal\fR
.RS 4
.PP
Distribute the parameter with respect to a mean value \fBmu\fR and a standard
deviation \fBsigma\fR as follow:
.PP
.RS 8
.nf
P(x) dx = 1/(log(\fBsigma\fR)*x*sqrt(2*PI) *
          exp(-(ln(x)-log(\fBmu\fR))^2 / (2*log(\fBsigma\fR)^2)) dx
.fi
.SH EXAMPLES
.PP
Soft particles are spheres whose radius is distributed according to an
histogram:
.PP
.RS 4
.nf
sphere:
  radius:
    histogram:
      lower: 1.0 # Min radius
      upper: 2.1 # Max radius
      probabilities:
        - 2
        - 1
        - 0.4
        - 1.23
        - 3
.fi
.RE
.PP
Soft particles are ellipsoids whose one of its semi-principal axis is
distributed with respect to a lognormal distribution:
.PP
.RS 4
.nf
ellipsoid:
  a: 1.0
  c:
    lognormal:
      sigma: 0.2
      mu: 1.3
.fi
.RE
.PP
Soft particles are ellipsoids whose semi\-principal axises are fixed. Its
volume is equal to the volume of an equivalent sphere whose radius follows an
histogram distribution:
.PP
.RS 4
.nf
ellipsoid:
  a: 1.1
  b: 0.3
  radius_sphere:
    histogram:
      lower: 1
      upper: 2.5
      probabilities: [ 0.5, 2, 1 ]
.fi
.RE
.PP
Soft particles are cylinders. Their radius is constant and their height is
distributed according to a gaussian distribution. The cylinder geometry is
discretized in 128 slices along 2PI:
.PP
.RS 4
.nf
cylinder:
  slices: 128
  radius: 1
  height: { gaussian: { mu: 1.3, sigma: 0.84 } }
.fi
.RE
.PP
Soft particles are cylinders whose height and radius are fixed. Their volume
is equal to the volume of a sphere whose radius is distributed with respect to
an histogram:
.PP
.RS 4
.nf
cylinder:
  height: 1.2
  radius: 3.4
  radius_sphere:
    histogram:
      lower: 1.24
      upper: 4.56
      probabilities: [ 2, 1.2, 3, 0.2 ]
.fi
.RE
.PP
Soft particle are helical pipes whose attributes are controlled by several
distribution types. Their helicoid curve is split in 256 steps while its
meridian is discretized in 128 slices:
.PP
.RS 4
.nf
helical_pipe:
  slices_helicoid: 256
  slices_circle: 128
  height : 4
  pitch : { gaussian: { mu: 3, sigma: 1.3} }
  radius_helicoid: { lognormal: { mu: 2, sigma: 0.4} }
  radius_circle:
    histogram:
      lower: 1
      upper: 1.5
      probabilities: [ 1, 1.2, 0.2, 0.5, 1.4 ]
.fi
.RE
.PP
Soft particles are supershapes whose 2 parameters of each of its superformulas
are controlled by gaussian distributions:
.PP
.RS 4
.nf
supershape:
  formula0:
    A: 1
    B: 1
    M: { gaussian: { mu: 5, sigma: 1 } }
    N0: 1
    N1: 1
    N2: { gaussian: { mu: 3, sigma: 1 } }
  formula1:
    A: 1
    B: 1
    M: { gaussian: { mu: 1.2, sigma: 0.3 } }
    N0: 1
    N1: 1
    N2: { gaussian: { mu: 1, sigma: 0.3 } }
.fi
.RE
.PP
Soft particles are supershapes with the same shape. Their volume is controlled
by an equivalent sphere whose radius follows a lognormal distribution:
.PP
.RS 4
.nf
supershape:
  formula0: { A: 1, B: 1,   M: 3, N0: 3, N1: 3, N2: 5 }
  formula1: { A: 2, B: 1.1, M: 3, N0: 1, N1: 1, N2: 1 }
  radius_sphere : { lognormal: { mu: 2.2, sigma: 1.3 } }
.fi
.RE
.PP
Soft particles are spheres and cylinders with 2 times more spheres than
cylinders. The cylinder parameters are controlled by lognormal distributions
and spherical soft particles have a fixed radius:
.PP
.RS 4
.nf
- sphere: { radius: 1.12, proba: 2.0, slices: 64 }

- cylinder:
    radius: {lognormal: { sigma: 2.3, mu: 0.2 } }
    height: {lognormal: { mu: 1, sigma: 1.5 } }
    slices: 32 # Discretisation in 32 slices
    proba: 1
.fi
.RE
.SH NOTES
.PP
[1] YAML Ain't Markup Language \- http://yaml.org
.SH SEE ALSO
.BR schiff (1)
