# Schiff

The purpose of this program is to estimate the radiative properties of soft
particles with an "Approximation Method for Short Wavelength or High-Energy
Scattering" (L. Schiff, 1956). It relies on the model detailed in "Monte Carlo
Implementation of Schiff's Approximation for Estimating Radiative Properties of
Homogeneous, Simple\-Shaped and Optically Soft Particles: Application to
Photosynthetic Micro-Organisms"
([Charon et al. 2015](http://www.sciencedirect.com/science/article/pii/S0022407315003283)).

## How to build

The library relies on the [CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) package to build. It also depends
on the
[LibYAML](http://pyyaml.org/wiki/LibYAML),
[RSys](https://gitlab.com/vaplv/rsys/),
[Star-3D](https://gitlab.com/meso-star/star-3d/),
[Star-Schiff](https://gitlab.com/meso-star/star-schiff/) and
[Star-SP](https://gitlab.com/meso-star/star-sp/) libraries.

First ensure that CMake is installed on your system. Then install the RCMake
package as well as all the aforementioned prerequisites. Then generate the
project from the `cmake/CMakeLists.txt` file by appending to the
`CMAKE_PREFIX_PATH` variable the install directories of its dependencies.

## How to use

Refer to the
[manual pages](https://gitlab.com/meso-star/schiff/tree/master/doc)
for informations on the schiff program, the list of its options and the file
formats on which it relies.

## Release notes

### Version 0.4.2

Sets the required version of Star-SampPling to 0.12. This version fixes
compilation errors with gcc 11 but introduces API breaks.

### Version 0.4.1

Displays the time spent constructing the geometric distribution, writing the
sampled geometries, and performing the calculations.

### Version 0.4

- Set the minimum number of scattering angles to 2 rather than the previous
  and arbitrary limit fixed to 100.
- Fix a bug in the generation of the shape of the helical pipe.
- Update the short help of the command as well as the man pages.
- Add the `-D` option that discards all computations for large scattering
  angles.
- Add the `--version` that prints the current version of the `schiff` command.

### Version 0.3.1

- Reformat the man pages.

## License

Copyright (C) 2020, 2021 [|Meso|Star>](http://www.meso-star.com)  
(<contact@meso-star.com>).  
Copyright (C) 2015, 2016 CNRS.

Schiff is free software released under the GPL v3+ license: GNU GPL version 3
or later. You are welcome to redistribute it under certain conditions; refer to
the COPYING file for details.
